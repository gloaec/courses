# TD - NoSQL/MongoDB

## Objectif

Découvrir la base de données NoSQL MongoDB. Elle appartient à la catégorie des
bases orientées documents, est développé par la société américaine 10Gen en
C++.

## Installation

- On récupère la version 3.0 (ou +) depuis le site www.mongodb.org/downloads.

OU

- Installation de MongoDB : <https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-16-04>

## Terminologies

- Un **document** est l'unité de base de données. Similaire à un tuple.
- Une **collection** peut être considérée comme une table dans le contexte SGBDR mais
  sans schéma et représentée en JSON.
- Une **base de données** est un ensemble de collections.

## Lancement

Dans le répertoire bin, on trouve plusieurs exécutables que nous utiliserons
pendant les sessions sur MongoDB. Le mode d'exécution de MongoDB est
client/serveur. Par défaut, le serveur écoute sur le port 27017 (et sur 28017
pour des info adminstratives). Pour que le lancement fonctionne, il est
nécessaire de créer un répertoire (par ex. data-mongodb dans votre répertoire
courant).  On lance le serveur avec l'exécutable 

~~~bash
mongod --dbpath repCourant/data­mongodb
~~~

Le shell de MongoDB  se lance avec l'exécutable mongo . C'est un shell
javascript (donc interprétation du code possible) qui permet d'administrer la
base et de lancer des requêtes.

### Exemples

~~~bash
>x=20
>x/4
5
> function facto(n) { if(n<=1) return 1; 
... return n * facto(n-1); 
... } 
> facto(5) 
120
~~~

## Shell

La commande help permet d'obtenir une liste de commandes disponibles :

~~~bash
show dbs, show collections, use <dbName>
~~~

## JSON

Dans un éditeur de texte:

1.Ecrire un document JSON comportant le nom 'davis, le prenom 'miles' et une
adresse comportant la rue, la ville et le pays
2. traduire le document XML suivant en utilisant un tablleau pour les
   téléphones:

~~~xml
<person> 
  <name>Coltrane</name> 
  <age>65</age> 
  <address> 
    <city>New York</city> 
    <postalCode>10021</postalCode> 
  </address> 
  <phones> 
    <phone type="home">111-555-1234</phone> 
    <phone type="mobile">116-555-1234</phone> 
  </phones> 
</person>
~~~

## Requête

3. A l'aide la commande db.person.save({..}), enregistrer dans la base 'esipe'
   et la collection 'person', le document de la question 1.
4. Requête pour obtenir un document

~~~javascript
db.person.findOne()
~~~

On remarque que le système ajoute un champ `_id` qui correspond à la clé
primaire du document.

## Modélisation

Dans le cadre de la modélisation d'une vidéothèque, on dispose du diagramme
entité association suivant :

\includegraphics[width=\textwidth]{uml.png}

On souhaite implanter une base de données similaire sur MongoDB. Pour cela, on
doit tenir tenir que cette base est sans schéma et que son langage de requête
ene permet pas de représenter des jointures. Il faut donc penser en terme de
dénormalisation dès l'étape de modélisation.

5. En considérant que l'application exploitant la base de données requête
   essentiellement sur les films et non sur les acteurs et réalisateurs,
   roposer une représentation au format JSON pour un document contenant les
   informations suivantes et l'implanter sur une nouvelle base de données (dbm)
   et une collection movie :

Le nom du film est "A History of Violence", année "2005", son sommaire "Tom
Stall, a humble family man and owner of a popular neighborhood restaurant,
lives a quiet but fulfilling existence in the Midwest. One night Tom foils
a crime at his place of business and, to his chagrin, is plastered all over the
news for his heroics. Following this, mysterious people follow the Stalls'
every move, concerning Tom more than anyone else. As this situation is
confronted, more lurks out over where all these occurrences have stemmed from
compromising his marriage, family relationship and the main characters' former
relations in the process.", c'est un film américain (country USA) et son genre
est "Crime". Son réalisateur est David Cronenberg, né en 1943 et les
principaux acteurs sont Ed Harris (né en 1950) dasn le rôle de "Carl Fogarty",
Vigo Mortensen (né en 1958)  dans le rôle de "Tom Stall", Maria Bello (1967)
dans le rôle de "Eddie Stall " et William Hurt (1950) cans le rôle de "Richie
Cusack".

6. Saisir le second film suivant :

"The Social network", 2010, Drama, "On a fall night in 2003, Harvard undergrad
and computer programming genius Mark Zuckerberg sits down at his computer and
heatedly begins working on a new idea. In a fury of blogging and programming,
what begins in his dorm room soon becomes a global social network and
a revolution in communication. A mere six years and 500 million friends later,
Mark Zuckerberg is the youngest billionaire in history... but for this
entrepreneur, success leads to both personal and legal complications.", USA.
Les acteurs sont : Jesse Eisenberg (1983) dans le rôle de Mark Zuckerberg",
Rooney Mara (1985) dans le rôle de Erica Albright. Le réalisateur est David
Fincher (1962) 

### A partir de cette base de données :

1. On obtient le nombre de documents dans la collection movies avec 

~~~javascript
db.movies.count()
~~~

2. Afficher les films de 2005. Entre les parenthèses de `find()`, on place
   un filtre au format JSON : `{year: "2005"}`. Faire en sorte que cela
   soit plus lisible.
3. Fournir l'ensemble les titres des films de 2005. Pour ne sélectionner
   que quelques attributs en sortie, on ajoute un filtre en seconde
   position de `find()`. Ce filtre comporte un couple clé/valeur du type
   `attribut:{0,1}` avec `1` si on veut l'affichage, `0` sinon. 
4. On obtient tout de même `_id`. Comment le supprimer avec ce que l'on
   vient de voir. 
5. Afficher le summary de "The Social network".
6. Qui est le directeur de "A History of Violence".
7. Afficher les acteurs de "The Social network"
8.Afficher la liste de films où Cronenberg est le nom du réalisateur. Donc
  cela revient filtrer dans un sous document (director). On utilise pour
  cela la forme objet que l'on utilise en javascript: cléDocument.attribut
  : `director.last_name : "Cronenberg"`. Afficher les noms (`last name`)
  des réalisateurs dont le prénom est David.
9. Afficher la liste des films où Mortensen est le nom d'un acteur. Plus
   difficile car on filtrer un sous document, élément d'un table. On
   utilise pour cela `$elemMatch` sous la forme : `{ arrayName : {$elemMatch
   : { cle : valeur, ..}}}`
10. Des opérateurs pour les comparaisons : `$gt`, `$gte`, `$lt`, `$lte` avec une
    syntaxe de la forme : `{clé : {$gte : valeur}}`. Afficher les films dont
    year est >= à 2005.
11. Afficher les noms des réalisateurs dont l'année de naissance est >=
    1960.
12. Avec l'opérator $exists on peut savoir si une clé se trouve dans un
    document. 


    C'est pratique puisque MongoDB est une BD sans schéma. Par exemple pour savoir
    s'il y a un clé `"country"` dans un document : `{country : {$exists : 1}}` (ou
    `true` à la place de `1`). Tester pour l'absence, on remplace `0` (`false`) par `1`.
    Afficher les noms des films pour lesquels il y a un réalisateur. 

13. Il y a d'autres opérateurs avec des `$`, par exemple `$in`, `$nin` (savoir
    si une valeur se trouve dans un tableau), `$all` (toutes les valeurs se
    trouvent dans un tableau), `$type` (test sur le type d'une valeur), `$not`,
    `$where`, `$or` et `$and`. Par exemple, les films américains ou bien du genre
    "Drama" : 
    
    ~~~javascript
    db.movies.find({$or: [{country:"USA"},{"genre":"Drama"}]},{title:1})
    ~~~

    Afficher les films du genre Drama ou bien ceux ayant un sommaire
    (summary).

14. On saisir un nouveau film "Into the Wild" de 2007, Drama dont le
    réalisateur est "Sean Penn". Vérifier combien il y a de doc dans la
    bd. Demander l'affichage des films.

15. On veut ajouter des données sur le Into The wild : le sommaire : 

    « After graduating from Emory University, top student and athlete
    Christopher McCandless abandons his possessions, gives his entire
    $24,000 savings account to charity and hitchhikes to Alaska to live in
    the wilderness. Along the way, Christopher encounters a series of
    characters that shape his life. ».
    
    Donc : 
    
    ~~~javascript
    movies.update({id du film},{summary : ".. "})
    ~~~

    Vérifier le document du film. Ouch !! Une solution à la Javascript: 

    ~~~javascript
    var into = db.movies.findOne({title : "Into the Wild "})
    ~~~
    
    et maintenant on ajoute les attributs que l'on veut : 
    
    ~~~javascript
    into.summary = "After.."
    ~~~
    
    On tape into pour vérifier. Ajouter un tableau
    vide pour actors. On valide en faisant : 
    
    ~~~javascript
    db.movies.update({id film},into)
    ~~~

16. Ajouter puis supprimer la clé actors : 

    ~~~javascript
    db.movies.update({..},{$unset : {actors:1}})
    ~~~

17. On va ajouter des acteurs au film Into the Wild: 

    ~~~javascript
    db.movies.update({
      "_id": ObjectId("50ad3ee118dd6a00e6fbce31")
    },{
      $addToSet: {
        actors: {
          "last_name":"Hirch",
          "first_name":"Emile"
        }
      }
    })
    ~~~

    Ajouter William Hurt comme acteur.

18. Pour supprimer un document :

    ~~~javascript
    db.movies.remove({id})
    ~~~

    Supprimer le film Into the Wild.

