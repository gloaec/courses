# TP - TODO Application avec MongoDB

## Objectifs

Créer une application web pour la gestion d'une liste TODO, dans le langage de votre choix (Javascript, Php, Perl, Ruby, Java...).

\includegraphics[width=\textwidth]{todo.png}

L'application doit présenter les fonctionnalités suivantes 

- Les utilisateurs n'ont pas besoin de s'authentifier, utilisation des cookies pour identifier l'utilisateur.
- Les utilisateurs doivent être en mesure de `create`, `read`, `update`, `delete` les éléments de sa liste de choses à faire.
     - `create`:
         - Ajouter un nouveau Todo pour l'utilisateur
     - `read`:
         - Afficher la liste complète des Todos de l'utilisateur
         - Filrer les Todos par état ("à faire", "fait")
     - `update`:
         - Modifier la description d'un Todo
         - Changer l'état d'un Todo ("à faire", "fait")
     - `delete`
         - Supprimer un Todo
         - Supprimer plusieurs Todo à la fois

## Pour aller plus loin (facultatif)

- Implémenter un méchanisme d'authentification sécurisé
- Implémenter la notion de Todos partagés entre utilisateurs
- Comment migrer la base MongoDB vers une architecture distribuée ?


## Environement technique

- Ubuntu 16.04
- `nodejs` et `npm` installés

## Documentation

- Installation de MongoDB : <https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-16-04>
- Implémentations MVC de l'application TODO : <http://todomvc.com/>
