# TP - Automatisation de tâches pour MongoDB

## Objectifs

### Execice 1

Créer un outil d'automatisation de l'import/export d'extractions MongoDB

L'outil sera scindé en 2 scripts:

#### Script d'export

- Un script d'export, générant un export JSON pour une base de données dont le
  nom aura été renseigné en paramètre. Les exports devront être stockés sous la
  forme d'archives compressées (`tar.gz` ou `zip`)
 
~~~bash
./export-database "<nom_base>"
~~~

#### Script d'import

- Un script d'import quie devra être capable de charger un fichier d'export en
  fonction du nom de la base et d'une date d'export (jour/mois/année). Si aucun
  export n'a été fait à cette date, le script devra le signaler.
 
~~~bash
./import-database "<date_export>" "<nom_base>"
~~~

L'arborescense de stockage des fichiers et la convention de nommage de ceux ci
est laissé à votre libre apréciation.

La configuration sera stockée dans un fichier à part (adresse IP de la base de
donnée, port, identifiant du compte administrateur à utiliser)

## Pour aller plus loin

- Intéger la synchornisation des répertoires des extractions avec un machine
  distante via `rsync`
- Chiffrer mes archives des exports

### Exercice 2

Créer un script d'automatisation d'une base de donnée MongoDB avec un compte
utilisateur associé.

Le script devra avoir les caractéristiques suivantes : configuration stockée
dans un fichier à part (adresse, données, port, identifiant du compte
administrateur à utiliser, la tailler minimum du mot de passe)

Le script devra être interactif et poser les questions suivantes:

- Quel est le nom de la base de donnée à créer ?
- Quel est le nom de l'utilisateur autorisé à utiliser la base de données ?
- Quel est le mot de passe de l'utilisateur ?
- Quelle est l'adresse IP d'origine autorisée pour l'utilisateur ?

Le script devra valider les données entrées: Le mot de passe de l'utilisateur
ne devra pas être inférieur à un certain nombre de caractères (renseigné dans
le configuration). Les noms de la base de donnée, de l'utilisateur et
l'addresseIP d'origine ne devront pas pouvoir etre vide. Le script devra
s'interrompre et ne pas laisser la base de données dans un état intermédiaire
si la procédure échoue (ex: utilisateur créé, mais base de donnée non créée car
conflit dans le nom)

## Environnement technique

- Ubuntu 16.04

**Les scripts peuvent être réalisés en Bash, Python, Perl ou Ruby, limités
à leurs librairies standards respectives** (pas de dépendance vers d)

## Documentation

- Installation de MongoDB : <https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-16-04>
