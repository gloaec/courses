---
title: "TP commun"
subtitle: "NFC Password Keychain"
output: pdf_document
urlcolor: blue
---

# Prérequis (fait, mais à lire)

## Préparation de la carte SD

Dans un premier temps, il va falloir installer le système d'exploitation
destiné à la RaspberryPi. Nous utiliserons la dernière de version de *Raspbian*
(Distribution *Debian* portée sur RaspberryPi).

**Archive fournie** : `2017-03-02-raspbian-jessie-lite.zip`

### Avec Linux

Formatter la carte SD au format FAT32, plusieurs possibilités :

- Utiliser parted (<http://www.cio.com/article/3176034/linux/how-to-format-an-sd-card-in-linux.html>)
- Utiliser Gparted ou le gestionnaire de disque du système

Dézipper l'image

    $ sudo apt-get install unzip
    $ unzip 2017-03-02-raspbian-jessie-lite.zip -d /tmp

Identifiez la partition ou copier l'image (correspondant à votre carte SD).

***Attention***: Veiller à bien identifier la bonne partition, vous risquez
sinon d'écrire sur le disque de la machine et la rendre complètement
inopérable.

Généralement, les supports amovibles (type carte SD)  sont montés dans `/media` ou `/mnt` :

~~~bash
$ lsblk
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
mmcblk0     179:0    0    15G  0 disk 
|-mmcblk0p1 179:1    0    63M  0 part /media/boot
|-mmcblk0p2 179:2    0  14,9G  0 part /media/8a9074c8-46fe-4807-8dc9-8ab1cb959010
nvme0n1     259:0    0   477G  0 disk 
|-nvme0n1p1 259:1    0   200M  0 part /boot/efi
|-nvme0n1p2 259:2    0 146,5G  0 part 
|-nvme0n1p3 259:3    0   293G  0 part /
|-nvme0n1p4 259:4    0  18,7G  0 part /var
|-nvme0n1p5 259:5    0   7,5G  0 part /var/log
|-nvme0n1p6 259:6    0   9,3G  0 part /tmp
|-nvme0n1p7 259:7    0   1,9G  0 part [SWAP]
~~~

Démontez les différentes partitions de la carte :

    $ sudo umount /media/boot
    $ sudo umount /media/8a9074c8-46fe-4807-8dc9-8ab1cb959010

Enfin, copier l'image sur la partition **primaire** du disque amovible :

    $ sudo dd if=/tmp/2017-03-02-raspbian-jessie-lite.img of=/dev/mmcblk0

### Avec Windows

Procédé identique, outils différents :

<https://sourceforge.net/projects/win32diskimager/files/latest/download>

## Configuration de la RaspberryPi

Une fois la RaspberryPi démarrée, lancez le
gestionnaire de configuration :

    $ sudo raspi-config

**Important** : Par défaut, le clavier sera au format *QWERTY* puisque la
langue utilisée est *US_en*. La première étape de configuration sera donc de
changer la langue pour faciliter un peu la suite :

### Changer la langue du clavier

Sélectionner :

`Localisation Options` > `Change Keyboard Layout` > `Generic 105-key (Intl) PC`
> `Other` > `French` > `French` > `The default for the keyboard layout` > `No
compose key`

Terminer la configuration et redémarrer ensuite la RaspberryPi afin que les
changements soient pris en compte :

    $ sudo reboot

### Configurer [SSH][ssh]

Activer OpenSSH afin de pouvoir vous connecter à la machine via le réseau :

    $ sudo raspi-config

Aller dans `Interfacing Options` > `P2 SSH` et sélectionner `<Yes>`.

Pas de redémarrage nécessaire.

### Configuration réseau

Votre RaspberryPi est configurée ainsi :

`/etc/network/interfaces`:

    auto wlan0
    allow-hotplug wlan0
    iface wlan0 inet static
         wpa-ssid "PiFi-1" # (ou PiFi-2)
         wpa-psk "raspberry"
         address 192.168.42.1XX #=> N° de kit
         netmask 255.255.255.0
         gateway 192.168.42.1
         wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

`XX` correspond à votre numéro de kit

Ex: $Kit = 8$, $IP = 192.168.42.108$

## Configuration du poste de travail (Début du TP)

Une autre RaspberryPi a été connectée au réseau cablé ethernet afin de servir
de passerelle vers le réseau WiFi. Ce réseau sans fil vous permettra de
communiquer avec votre RaspberryPi avec [SSH][ssh].

### Avec Linux

Ajouter un nouvelle route vers le réseeau Wifi via la RaspberryPi "passerelle":

    ip route add 192.168.42.0/24 via X.X.X.X

`X.X.X.X` (IP DHCP de la passerelle sur `eth0`)

### Avec Windows

FIXMEMAYBE

## Connexion SSH

Connectez vous à votre RaspberryPi en ssh :

- Windows : Utiliser *Putty* ou équivalent
- Linux : `ssh pi@192.168.42.1XX`

Mot de passe : `raspberry`

Lorsque le prompt s'affiche, **changez immédiatement votre mot de passe** :

    $ passwd


## Configuration de l'extension EXPLORE-NFC-WW

### Activer la liaison [SPI][spi]

Par défaut, la liaison [SPI][spi] est désactivée. Ce bus de données est
nécéssaire afin de permettre à la board de communiquer avec l'extension NFC
(via les GPIOs). Utilisez l'une des deux méthodes proposées :

#### Méthode 1 (Raspbian Jessie)

Activer la liaison [SPI][spi] à l'aide du gestionnaire de
configuration :

    $ sudo raspi-config

Aller dans `Interfacing Options` > `P4 SPI` et sélectionner `<Yes>`.

**Redémarrer** la RaspberryPi convenablement.

#### Methode 2 (Universelle)

La RaspberryPi n'a pas de BIOS comme dans les machines traditionnelles, mais
elle met à disposition un [fichier de configuration
`/boot/config.txt`][config.txt] qui sera lu au démmarrage de la board.

Editer le fichier [`/boot/config.txt`][config.txt] et décommenter/ajouter la ligne
suivante afin de charger l'interface matérielle SPI :

    dtparam=spi=on

**Note** : Utiliser l'éditeur de texte de votre choix, les éditeurs suivants
sont installés : `vi`, `vim.tiny` et `vim` (bon OK, et `nano`)

Enregistrer le fichier et **redémarrer** la RaspberryPi convenablement.

---

# Exercice 1

## Installation des binaires pour EXPLORE-NFC-WW

1. Connectez-vous à la RaspberryPi en [SSH][ssh]
2. Dézippez l'archive `$HOME/SW282715.zip` (`man unzip`)
3. Installez les différents paquets dans cet ordre (`man dpkg`)  :

      - `libneardal0_0.14.2-1_armhf.deb`
      - `libwiringpi2-2.25-1_armhf.deb`
      - `neard-explorenfc_0.9-1_armhf.deb`


## Raccordement de l'extension

À partir du guide officiel du constructeur *NXP* :

<http://www.nxp.com/documents/application_note/AN11480.pdf>

1. Éteindre convenablement la RaspberryPi et la mettre hors tension afin de
   pouvoir connecter l'extension. (**à faire valider**)
2. Connectez (physiquement) l'extension à votre RaspberryPi (**à faire valider**)
3. Remettre la RaspberryPi sous tension.

## Tester le logiciel fournit par le constructeur

1. Écrivez sur la carte *NFC Sample card* la chaine de caractères suivante : "`Hello World`"
2. Effectuez un lecture la carte et constater que l'écriture n'a pas fonctionné.

*Note* : L'installation du logiciel fourni par le constructeur a pris plus de
6h, c'est pourquoi vous serez épargnés de le faire. D'autant plus que, entre :

- le manque cruel de documentation,
- les pages 404 omniprésentes sur les sites de Element14 et NXP
- la nécessité et l'impossibilité de s'inscrire sur le site du constructeur pour
  télécharger les binaires,
- un demi-douzaine de versions installées aux comportements aléatoires,
  incomplets voire destructifs et en totale inadéquation avec la
  "documentation",
- une compilation désespérée de la dernière version des sources publiées et de
  ses nombreuses dépendances ...
- ... la frustration du `Segmentation Fault` après 2h de compilation acharnée...

... impossible d'obtenir un binaire capable d'écrire sur un tag (lui aussi
fourni par le constructeur `-_-'`).

**Question : En quoi compiler un programme ou un librairie depuis les sources
les plus récentes semble être une mesure désespérée et ne devrait jamais être
fait à des fins autres que démonstratifs ?** (*Répondre dans le fichier* `$HOME/README`)

---

# Exercice 2

Le logiciel fourni par le contructeur ne fonctionne pas `:/` Allons bon. Que
faire dans ces cas là ? Allons voir du coté de Python et NodeJS :

- <https://github.com/JohnMcLear/NXP-Explore-NFC-nodejs-wrapper>
  Oh super ! Une surcouche d'un truc qui fonctionne pas, nous voilà bien avancés !
  (Testé, non fonctionnel et au vu du code ce n'est pas très étonnant)

**Question: Pourquoi est-ce une mauvaise idée d'utiliser des librairies de type
"wrapper" qui interface une commande invoquée en "parsant" les données
directement sur les sorties standards (`stdout`, `stderr`) ?**

- <https://github.com/svvitale/nxppy>
  Aaaaahh ! Une bonne vielle librairie Python codée en C, qui semble marcher

Allons-y !

## Installation de la librairie

Les RaspberryPi ne sont pas équipées d'internet comme vous avez pu déjà le
constater. Inutile donc de suivre la procédure fournie sur github. Le dépôt
est déja cloné sur la RaspberryPi, il suffit donc d'installer la lib :

    $ cd $HOME/nxppy
    $ sudo python setup.py build install

Pour la librairie permettant de "parser" les block (comme dans l'exemple sur
github) :

    $ cd $HOME/ndeflib
    $ sudo python setup.py build install

## Scripts

La librairie respecte la norme [ISO14443-3][iso], ce qui semble plutôt en
adéquation avec les [caractéristiques des badges NTAG216][nfc_specs]. Mais,
accrochez vous, parce que c'est du bas niveau...

1. Écrire un script `read.py` capable de lire et d'afficher l'intégralité du
   contenu d'un badge NTAG216 déposé à l'avance sur le capteur
2. Écrire un script `poll.py` qui lit des badges et affiche
   leurs `uid` à la volée.
3. Écrire un script `write_text.py <text>` capable d'écrire sur un badge NTAG216 un
   contenu de type *Text* passé en argument
4. Écrire un script `write_uri.py <uri>` capable d'écrire sur un badge NTAG216
   un contenu de type *URI* passé en argument. (L'ouverture automatique du lien
   au contact avec smartphone équipé NFC serait un plus)

---

# Exercice 3

Le but de ce TP est de proposer une implémentation littérale d'un [porte clés
de mots de passes (password keychain)][keychain]. Ce genre d'outil provient de
la nécessité actuelle d'utiliser un multitude de mots de passe différents afin
de limiter les conséquences engendrées par la révélation publique de l'un
d'entre eux. Ce genre d'utilitaire est né dans le but de stocker une variété de
mots de passes, sans avoir à tous les mémoriser. Pour des besoins de mobilité,
ces applications se sont vues portée sur smartphone et posent aujourd'hui de
sérieux problèmes de vulnérabilité. C'est pour quoi il semblerait judicieux de
matérialiser et par conséquent "déconnecter" de l'IoT ces trousseaux de mots de
passes. Quoi de mieux qu'un petit porte clé NFC ou un carte de plus dans le
porte feuille encore indémodable ?

On propose d'utiliser le gestionnaire de mots de passes [pass][pass].

1. Créer un trousseau de mots de passe avec au moins un niveau et un tuple
   identifiant/mdp. (`man pass`, `man gpg`)
2. Il s'agit de données très sensibles, un accent tout particulier doit être mis
   sur la sécurité.
   
    - **Question: Proposez un approche technique pour sécuriser les données qui
seront stockées sur le tag**

    - **Question: Quelle sera la principale limitation avec l'utilisation de tags NTAG216** ?

3. Écrire un script `write_pass.py` capable de stocker le trousseau de mots
   passes de manière sécurisée sur un tag NFC.
4. Écrire un script `read_pass.py` capable de rapatrier le trouseau de mots de
   passes présent sur un tag NFC


[spi]: https://fr.wikipedia.org/wiki/Serial_Peripheral_Interface "Serial Peripheral Interface"
[config.txt]: https://www.raspberrypi.org/documentation/configuration/config-txt/ "config.txt"
[ssh]: https://fr.wikipedia.org/wiki/Secure_Shell "Secure Shell"
[iso]: http://jpkc.szpt.edu.cn/2007/sznk/UploadFile/biaozhun/iso14443/14443-3.pdf "ISO14443-3"
[nfc_specs]: http://www.nxp.com/products/identification-and-security/smart-label-and-tag-ics/ntag/nfc-forum-type-2-tag-compliant-ic-with-144-504-888-bytes-user-memory:NTAG213_215_216#featuresExpand "NFC Tags Specs"
[keychain]: https://en.wikipedia.org/wiki/Keychain_(software) "Keychain"
[pass]: https://www.passwordstore.org/ "Pass"

<!--
  vim: spell spelllang=fr
-->
