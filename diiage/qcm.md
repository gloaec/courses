# QCM

## Base de données: Relationnelles, NoSQL

### 1. Quelle est la principale raison à l'origine du besoin pour une autre solution de stockage de donnée que le modèle relationnel classique ?

1. Le langage de requêtage SQL devenu trop complexe
2. La quantité trop importante de données à stocker
3. L'impossibilité pour le modèle relationnel de stocker des documents
4. L'impossibilité pour le modèle relationnel de stocker des tuples clé-valeur

Bonne(s) réponse(s): 2

### 2. Pourquoi les bases de données objets n'ont jamais gagné en popularité dans les années 2000 ?

1. A cause de SQL, devenu mécanisme d'intégration systématique
2. A cause de l'absence d'intégrité relationnelle
3. A cause de l'absence de langage de requêtage

Bonne(s) réponse(s): 1

### 3. Que signifie ACID: l' ensemble de propriétés qui garantissent qu'une transaction informatique est exécutée de façon fiable ?

1. Appartenance, Consistence, Intégrité, Dynamisme
2. Approvisionnement, Conditionnement, Irréversibilité, Dimensionnement
3. Atomicité, Cohérence, Isolation, Durabilité
4. Asservitude, Commutation, Indépendance, Dissociabilité

Bonne(s) réponse(s): 3

### 4. Quels mécanismes sont utilisés pour améliorer la scalabilité des bases de données ?

1. La distribution des traitements
2. La distribution des données
3. La distribution des inodes

*Bonne(s) réponse(s)*: 1,2

### 5. Parmi les propositions suivantes, quelles sont les modèles de base de données non relationnelle valides ?

1. Orienté document
2. Orienté graphe
3. Orienté colonne
4. Orienté clé/valeur

*Bonne(s) réponse(s)*: 1,2,3,4

### 6. Sélectionner les propositions valides: Le modèle de traitement "MapReduce" est...

1. ... peu adapté à la parallélisation.
2. ... inspiré des principes de programmation fonctionnelle.
3. ... permet de profiter des index de manière plus performante.

*Bonne(s) réponse(s)*: 2

### 7. Suivant l'infrastructure ci dessous, quelles sont les requêtes à préconiser d'un point de vue sécurité et performance pour l'ouverture de droits d'accès en lecture sur la base de données `app1`, hébergée sur la machine A pour des processus s'exécutant sur la machine B ?

* Machine A
    * Nom de domaine: mysql.lan
    * IP: 192.168.0.2
    * Services: MySQL
* Machine B
    * Nom de domaine: web.lan
    * IP: 192.168.0.3
    * Services: Apache2, PHP-FPM

1. `GRANT ALL PRIVILEGES on app1.* TO 'my_new_user'@'%' IDENTIFIED BY 'my_user_password';`
2. `GRANT SELECT on app1.* TO 'my_new_user'@'%' IDENTIFIED BY PASSWORD 'my_user_password';`
3. `GRANT SELECT on app1.* TO 'my_new_user'@'192.168.0.3' IDENTIFIED BY 'my_user_password';`
4. `GRANT SELECT on *.* TO 'my_new_user'@'web.lan' IDENTIFIED BY 'my_user_password';`

*Bonne(s) réponse(s)*: 3

### 8. Quelles sont les caractéristiques clés des systèmes distribués qui constituent aujourd'hui le socle de la scalabilité horizontale des bases de données ?

1. Ils sont tolérants à la panne d'un ou plusieurs de leurs noeuds
2. Ils assurent une cohérence totale des données présentes dans le cluster
3. Ils peuvent accueillir un nouveau noeud sans interruption du cluster
4. Ils répartissent automatiquement les utilisateurs en fonction de leurs droits d'accès

*Bonne(s) réponse(s)*: 1,3

### 9. Comment appelle-t-on un réseau de serveurs qui ne comminiquent que par envoi de messages (ils ne partagent pas de disques, ni de ressouces de calcul) ?

1. Architecture message only
2. Architecture share nothing
3. Architecture broadcast only
4. Architecture share message

Bonne(s) réponse(s): 2

### 10. Qu'est ce que le Sharding ?

1. La distribution des traitements sur plusieurs serveurs
2. Un algorithme de compression des données
3. Le partitionnement des données sur plusieurs serveurs
4. Un cluster de bases de données

Bonne(s) réponse(s): 3.4

### 11. Pour quelles besoins préconiseriez vous une BDD NoSQL de type "Clé-Valeur" ?

1. Gérer des profils, des préférences utilisateurs
2. Calculer des données géospatiales
3. Implémenter un systeme de cache
4. Stocker les données d'un capteur

Bonne(s) réponse(s): 1,3,4

### 12. Pour quelles besoins préconiseriez vous une BDD NoSQL de type "Colonne" ?

1. Gérer des compteurs
2. Analyser des données semi-structurées
3. Proposer un service de routage
4. Journaliser des événements

Bonne(s) réponse(s): 1,2,4

### 13. Pour quelles besoins préconiseriez vous une BDD NoSQL de type "Document" ?

1. Proposer un catalogue de produit
2. Gérer les données d'un panier d'achat
3. Implémenter un systeme de détection de fraude
4. Implémenter un systeme de gestion de contenus

Bonne(s) réponse(s): 1,4

### 13. Pour quelles besoins préconiseriez vous une BDD NoSQL de type "Graphe" ?

1. Implémenter un moteur de recommandations
2. Gérer un réseau social
3. Analyser le web sémantique
4. Proposer un service de cartographie

Bonne(s) réponse(s): 1,2,3,4

## IoT

### 1. Quelles sont les particularités introduites par l'IoT dans les systèmes d'information ?

1. Une hétérogénéité de l'environnement
2. De multiples canaux de transports de l'information
3. Une puissance de calcul distribuée

*Bonne(s) réponse(s)*: 1,2

### 2. A quels principaux défis doit faire face le monde de l'IoT aujourd'hui ?

1. La sécurisation et la mises à jour des plateformes sur le long terme
2. La gestion des terminaux mobiles
3. La limitation des capacités de calcul des objets connectés

*Bonne(s) réponse(s)*: 1

### 3. Quelles sont les protocoles de transports les plus répandus aujourd'hui dans l'IoT, dans le segment "local" de communication ?

1. Bluetooth
2. Wifi
3. 4G

*Bonne(s) réponse(s)*: 1,2

### 4. Quelles chaînes illustrent le mieux le schéma général d'interconnexion d'un système IoT ?

1. Objet connecté -> Relai télécom -> Terminal mobile utilisateur
2. Objet connecté -> Passerelle Internet -> Plateforme vendeur -> Terminal mobile utilisateur
3. Objet connecté -> Serveur personnel utilisateur

*Bonne(s) réponse(s)*: 2

### 5. Avec quelles autres tendances actuelles l'IoT partage t-il des points communs d'un point de vue sécurité au sein du système d'information ?

1. Le B.Y.O.D.
2. Le travail à distance
3. L'intelligence artificielle

*Bonne(s) réponse(s)*: 1

### 6. Quelles plateformes sont aujourd'hui prédomminantes comme base pour la création d'un objet connecté ?

1. Android
2. GNU/Linux
3. Microcontroleurs

*Bonne(s) réponse(s)*: 1,2,3

### 7. De manière générale, sur quels critères un ingénieur devrait baser son choix lors de la séléction d'une solution IoT existante pour un projet ?

1. La license du projet et les conditions d'accès aux sources
2. L'origine de fabrication des composants matériels
3. La publication en OpenHardware des schémas de la plateforme
4. La pérennité du constructeur

*Bonne(s) réponse(s)*: 1,3,4

### 8. Quelles sont les prochaines étapes du développement de l'IoT dans les années à venir ?

1. La coopération et le partage effectif d'informations entre les objets connectés
2. L'assimilation des objets connectés dans les processus humains quotidiens
3. L'historisation des évènements physiques

*Bonne(s) réponse(s)*: 1,2

## 9. Comment appelle-t-on la phase de pré-traitement des données captées, juste avant qu'elles soient envoyées sur le Cloud ?

1. Edge computing
2. Cloud computing
3. Fog computing
4. Post computing

Bonne(s) réponse(s): 1,3

### 10. Comment appelle-t-on du code embarqué sur un microcontroleur ?

1. Un software
2. Un firmware
3. Un microware
4. Un hardware

Bonne(s) réponse(s): 3

### 11. À l'aide uniquement des GPIO pins d'une board, est-il possible de faire varier l'intensité d'un LED ?

1. Non, on ne peut que l'allumer ou l'éteindre
2. Oui, en modifiant le voltage du signal envoyé
3. Oui, en modifiant l'intensité du signal envoyé
4. Oui, en modifiant la modulation du signal envoyé

Bonne(s) réponse(s): 1

### 12. À l'aide uniquement des PWM pins d'une board, est-il possible de faire varier l'intensité d'un LED ?

1. Non, on ne peut que l'allumer ou l'éteindre
2. Oui, en modifiant le voltage du signal envoyé
3. Oui, en modifiant l'intensité du signal envoyé
4. Oui, en modifiant la modulation du signal envoyé

Bonne(s) réponse(s): 4

### 13. Faut-il prendre des mesures particulières pour connecter un servomoteur sur des GPIOs ?

1. Aucune, le moteur pourra tourner dans les deux sens
2. Vérifier la polarité dans le branchement pour que le moteur tourne dans le bon sens
3. Brancher une résistance en série pour éviter un court circuit
4. Brancher une résistance en parallèle pour éviter un court circuit

Bonne(s) réponse(s): 2

### 14. Quel intervalle de valeurs peut mesurer un ADC avec 8 bits par échantillon ?

1. 0 --> 1
2. 0 --> 8
3. 0 --> 255
4. 0 --> 256

Bonne(s) réponse(s): 3

### 15. Pour quel(s) besoin(s) préconiseriez vous un micro-controleur type "Arduino" ?

1. Capter des données rapidement
2. Contrôler du hardware
3. Traiter des données provenant de capteurs
4. Stocker une quantité importante de données

Bonne(s) réponse(s): 1,2

### 16. Pour quel(s) besoin(s) préconiseriez vous un micro-ordinateur type "Raspberry Pi" ?

1. Capter des données rapidement
2. Contrôler du hardware
3. Traiter des données provenant de capteurs
4. Stocker une quantité importante de données

Bonne(s) réponse(s): 3

## UML

## Sécurité des applications Web

### 1. Parmi les propositions suivantes, lesquelles ne désignent pas des catégories de failles d'applications Web ?

1. AJAX
2. REST
3. SQLI

*Bonne(s) réponse(s)*: 1,2

### 2. Quels sont les principaux objectifs liés à l'exploitation d'une faille de type "XSS" ?

1. Exécuter du code arbitraire sur le serveur d'une application
2. Voler des informations secrètes à un utilisateur
3. Effectuer des actions automatisées en se faisant passer par un utilisateur

*Bonne(s) réponse(s)*: 2,3

### 3. Quels sont les principales mauvaises pratiques de développement qui amènent à l'apparition de failles de type "injection" dans la base de données d'une application ?

1. Une mauvaise gestion des entrées utilisateur par l'environnement applicatif
2. Une mauvaise gestion des erreurs internes à l'application
3. Une méconnaissance du fonctionnement interne des librairies "connecteurs"

*Bonne(s) réponse(s)*: 1,3

### 4. Quelles sont les différences fondamentales entre une requête HTTP "POST" et "GET" dans le cadre général des failles de type "CSRF" ?

1. Les requêtes POST sont chiffrées par les navigateurs
2. Une requête POST ne peut pas être initiée par une balise HTML `<a />` sans avoir recours à Javascript
3. Il n'y a aucune différence

*Bonne(s) réponse(s)*: 2

### 5. Quel mécanisme d'authentification est aujourd'hui communément utilisé pour sécuriser l'accès à une API Web ?

1. XMLAuth
2. JSON Web Token
3. Shibboleth

*Bonne(s) réponse(s)*: 2

### 6. Le stockage d'une phrase de passe, pour être sécurisé, doit:

1. Être haché
2. Êtré salé
3. Être chiffré

*Bonne(s) réponse(s)*: 1,2

### 7. Dans le cadre du développement d'une application web, la sécurité de l'infrastructure devraient être abordée:

1. Dès les premières itérations de développement
2. En conjugant le travail des équipes de développement et les équipes de suivi de production
3. Analysée par une entité externe à la fin de chaque cycle de développement

*Bonne(s) réponse(s)*: 1,2,3

### 8. Via l'URL suivante, quels type de faille l'attaquant cherche t'il a exploiter?

`http://monsite.com/videos?p=%2e%2e%2f%2e%2e%2f%2e%2e%2f%2e%2e%2f%2e%2e%2f%2e%2e%2f%2e%2e%2f/etc/passwd`

1. Path traversal
2. XSS
3. Injection SQL

*Bonne(s) réponse(s)*: 1

### 9. Quelles sont les méthodes de mitigation utilisées pour limiter l'impact d'une faille de type RCE ?

1. Utilisation de processus CGI pour externaliser les traitements personnalisées
2. Utilisation d'un système de liste blanche pour valider les entrées de l'utilisateur
3. Utilisation d'un utilisateur système avec droits limités pour exécuter les tâches d'exécution modifiables par l'utilisateur

*Bonne(s) réponse(s)*: 2,3

### 10. Quels types de logiciels sont utilisés pour analyser et détecter les intrusions sur un système d'information ?

1. Les N.I.D.S.
2. Les P.I.D.S.
3. Les H.I.D.S.

*Bonne(s) réponse(s)*: 1,3

### 11.
