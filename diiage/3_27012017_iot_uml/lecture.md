# L'Internet des Objets

---

# État des lieux, historique et définitions

---

# Structuration

<img src="iot.svg" width="100%" height="100%"></img>

---

# Quels sont les enjeux ?

---

# Les couches matérielles

## Objets connectés

## Passerelles

## Serveurs

---

# Les couches logicielles

## Objets connectés

## Passerelles

## Serveurs

## Terminaux de consultation

---

# Et la sécurité dans tout ça ?
