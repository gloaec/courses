---
title: NoSQL
subtitle: Introduction
author: Ghislain Loaec
date: '`r format(Sys.Date(), "%d %B %Y")`'
lang: en-US
output:
  beamer_presentation:
    theme: cadoles
    template: includes/beamer.template2.tex
    latex_engine: xelatex
    highlight: zenburn
    toc: false
    slide_level: 2
  html_document:
    toc: true
    css: styles.css
  pdf_document:
    latex_engine: xelatex
---

# Introduction

---

## Un peu d'histoire

### 1980-1990 : Arrivée du modèle relationnel

- Persistance
- Intégration
    - Classe => Table
    - Attribut => Colonne (Champ)
    - Objet => Ligne (Enregistrement)
    - Héritage
    - Intégrité Relationnelle
    - Typage fort
- Transaction
- Un langage: SQL
- Journalisation

<div class="notes">
<dl>
<dt>Persistance</dt>
<dd>On peut stocker des information sur le disque.</dd>
<br>
<dt>Intégration</dt>
<dd>L'idéé ici, c'est vraimment de mapper (cartographier) les objet en
mémoire afin de pouvoir les manipuler au plus proche de comme il sont en
mémoire.</dd>
<br>
<dt>Transaction</dt>
<dd>Gérer la concurrence</dd>
<br>
<dt>SQL</dt>
<dd>Pas standard encore à l'epoque, mais assez standard pour favoriser la
communication</dd>
<br>
<dt>Journalisation</dt>
<dd>Pourvoir reprendre les transactions apres une interruption inopinée,
un coupure de courant....</dd>
</dl>
<p>
  Globalement en programmation, on assemble des structures d'objets dans
  la mémoire,  ce qui constitue <b>un ensemble cohérent de choses</b>.<br>
  Pour le stocker dans la base de données, nous devons <b>découper
  l'information en morceaux</b>, en bits, <br>
  de manière à ce qu'elle soient stockée dans des enregistrements dédiés
  et dans des <b>tables distinctes</b>.  <br>
  Une seule structure logique doit être traité de tel
  manière qu'elle est <b>éparpillée</b> dans une multitude de tables.<br>
  Ce problème est connu sous le nom d'<b>incompatibilité d'impédance.</b>
</p>
</div>

---

## Un peu d'histoire

------------- ----------------------------
 **Problème** Incompatibilité d'impédance
----------- ----------------------------

<div class="notes">
Le fait qu'on ait <b>deux modèles bien distincts</b> de comment voir les
choses, et le fait que l'on soit obligé de <b>les faire correspondre</b>
engendre des <b>difficultés</b>. C'est cela qui nous a conduit à avoir
des <b>frameworks de mapping relationnel objet</b>.
</div>

---

## Un peu d'histoire

### 1990-2000 : Arrivée des bases de données objet

<div class="notes">
  Cette incompatibilité d'impédance est devenu un problème tellement
  gênant, qu'au milieu des années 90, les gens se sont dit : "Eh bien,
  on pense que les base de données relationnelles sont <b>vouées à
  disparaître</b>, les bases de données <b>orientées objet</b> vont
  faire leur apparition, de cette manière, on peut prendre nos <b>objets
  en mémoire</b> et le sauvegarder <b>directement sur le disque</b>,
  sans être obligé de passer par un <b>transformation (mapping)</b>
  entre les deux.
</div>

- Stockage d'objets complexes
    - Aucun mapping des objets
- Pas de language de requêtage 
    - Récupération d'objets clé/valeur
- Pas d'intégrité relationnelle
    - Néanmoins un objet peut contenir une référence vers un autre objet

---

## Un peu d'histoire

-----------------------------------
 Pendant ce temps, SQL s'impose...
-----------------------------------

<div class="notes">
  Mais, on sait ce qu'il s'est passé ensuite, les bases de données objet
  n'ont <b>jamais vraiment vu le jour</b> et les gens qui ont cru que
  cela deviendrait la technologie dominante dans le futur se sont
  <b>lourdement trompés</b>.<br> On argumente sans cesse sur le pourquoi
  du comment, les bases de données orientées objets n'ont en vérité
  <b>jamais satisfait ce potentiel</b>.<br> Je pense qu'au coeur de
  cette reflexion reside le fait que <b>les base de données SQL sont
  devenues un mécanisme d'intégratio</b>.<br> Beaucoup de gens se sont
  mis à intégrer des variétés d'application à l'aide des bases de
  données SQL, presque <b>systématiquement</b>.<br> Et cette telle
  popularité a eu pour effet de ne <b>pas laisser beaucoup de place pour
  des technologie émergentes</b>.<br> Donc toujours au milieu des années
  2000, la tendance dominante technologique s'est <b>poursuivi avec le
  relationnel</b>
</div>

---

## Un peu d'histoire

### 2000-2010 : Dominance du modèle relationnel

<div class="notes">
  A partir de la, le modèle relationnel est largement <b>dominant depuis
  20 ans</b>, dans les entreprises, dans la recherche, que tout le monde
  on est obligé de l'utiliser. <br> Ce qui a changé véritablement, c'est
  la montée de l'Internet et tout particulièrement, des <b>sites qui
  génèrent enormément de traffic</b> : <br> les gros sites internets,
  tels que <b>Amazon</b> ou <b>Google</b>, etc.<br> A partir du moment
  ou tu as une grande quantité de traffic qui vient manipuler des
  donnees,<br> comment tu fais quand tu as besoin de <b>mettre les
  choses à l'echelle</b> ?<br> La premiere approche évidente serait  ...
  ?<br><br> <b>D'augmenter la puissance</b> en achetant des machines
  plus grosses...<br> Mais cette approche a des problèmes : non
  seulement elle <b>coûte chère</b> et il y a des <b>limites</b> bien
  définies de jusqu'ou on peut aller.<br> Donc, comme vous le savez
  tous, les <b>grandes organisations</b>, comme Google pour le plus
  connue, utilisent un <b>approche complètement différente</b> :</br>
  tout plein de petites machines, des cpus, des cartes mères, des
  disques durs, et on met tout ca dans la matrice.<br> Mais cette
  approche présente des <b>problèmes de stockage</b> avec l'utilisation
  de <b>SQL</b>. <br> SQL a été crée dans un <b>cadre d'usage bien
  précis</b> : <br> sur une grosse machine qui serait le noeud de
  données d'un système.<br> Ce n'est pas fait pour fonctionner sur un
  cluster de machines.<br> Et donc, la plupart des adeptes du <b>Big
  Data</b> ont compris ce problème, et il ont essayé,<br> il ont fait
  des tentatives d'adaptation du système de base de donnée
  <b>relationnel classique</b> pour le faire fonctionner de manière
  <b>distribuée</b>,<br> et le résultat laisser toujours à désirer
  <em>(retour d'expérience oracle distribué en L3).</em> <br>Le terme qui
  revient de ceux qui ont tenté l'expérience  de distribuer des bases de
  données relationnelles est: <b>"non naturel"</b>.<br> C'est très
  difficile à faire.
</div>


---

## Un peu d'histoire

---------------------------
 **Probleme**: scalabilité
---------------------------

<div class="notes">
  C'est les grandes entreprises du web amenées à traiter des <b>volumes
  de données très importants</b> <br>qui ont été les premières
  confrontées aux <b>limitations des SGBD relationnels</b>
  traditionnels. <br>Ces systèmes étant fondés sur une application
  stricte des <b>propriétés ACID</b>,<br> et généralement conçus pour
  fonctionner sur des <b>ordinateurs uniques</b>, ont rapidement posé
  des problèmes de <b>scalabilité.</b>
</div>

---

## The law of the hammer

> If the only tool you have is a hammer,\
> everything looks like a nail. [^maslow]

[^maslow]: \tiny Abraham Maslow - The psychology of science - 1996

---

## The law of the relational database

> If the only tool you have is a relational database,\
> everything looks like a table. [^walk]

[^walk]: \tiny A Walk in Graph Databases - 2012

---

### ACID

<div class="notes">
<b>ACID</b> (atomicité, cohérence, isolation et durabilité) sont un <b>ensemble
de propriétés</b> qui garantissent qu'une <b>transaction</b>
informatique est exécutée de façon <b>fiable</b>.
</div>

> - Atomicité

<div class="notes">
  La propriété d'atomicité assure qu'une transaction se fait compètement ou pas
  du tout : <br>si une partie d'une transaction ne peut être faite, il
  faut effacer toute trace de la transaction et remettre les données
  dans l'état où elles étaient avant la transaction. <br>L'atomicité
  doit être respectée dans toutes situations, comme une panne
  d'électricité, une défaillance de l'ordinateur, ou une panne d'un
  disque magnétique.
</div>

> - Cohérence

<div class="notes">
  La propriété de cohérence assure que chaque transaction amènera le
  système d'un état valide à un autre état valide. <br>Tout changement à
  la base de données doit être valide selon toutes les règles définies
  :<br>les contraintes d'intégrité, les rollbacks en cascade, les
  déclencheurs de base de données, et toutes combinaisons d'événements.
</div>

> - Isolation

<div class="notes">
  Toute transaction doit s'exécuter comme si elle était la seule sur le
  système. <br> <b>Aucune dépendance possible entre les
  transactions.</b> <br>La propriété d'isolation assure que l'exécution
  simultanée de transactions produit le même état que celui qui serait
  obtenu par l'exécution en série des transactions.
</div>

> - Durabilité

<div class="notes">
 La propriété de durabilité assure que lorsqu'une transaction a été
 confirmée, elle <b>demeure enregistrée</b><br> même à la suite d'une
 panne d'électricité, d'une panne de l'ordinateur ou d'un autre
 problème.<br> Par exemple, dans une base de données relationnelle,
 lorsqu'un groupe d'énoncés SQL ont été exécutés,<br> les résultats
 doivent être enregistrés de façon permanente,<br> même dans le cas
 d'une panne immédiatement après l'exécution des énoncés.
</div>

## Un peu d'histoire

### Ces dernières années

  - **Google**: Bigtable (2005)
  - **Amazon**: Dynamo (2004-2007)
  - Ubuntu One: CouchDB (2005)
  - Facebook: Cassandra (2008) puis HBase
  - **LinkedIn**: Project Voldemort (2009)
  - Sourceforge.net: MongoDB (2009)
  - ...

<div class="notes"> 
  Donc, une paire d'organisation (Google + Amazon) se sont dis : <br> "On
  en a marre de ce bordel, on doit essayer quelque chose de
  différent",<br> et ils ont développés leur propre systemes de stockage
  de données,<br>très différents de l'approche relationnelle classique.
  <br>Et ils ont commencé à en parler un peu, à publier des papiers.
  <br>Et c'est ça qui a véritablement inspiré un tout nouveau mouvement
  pour des base de données,<br> que l'on appelle le <b>"NoSQL
  Mouvement"</b> 
</div>>

# NoSQL

<div class="notes">
  La rencontre de 2009 à San Francisco est considérée comme l'inauguration de
  la communauté des développeurs de logiciels NoSQL.<br>Plus de 100 développeurs
  de logiciels ont assisté à des présentations de solutions telles que Project
  Voldemort, Cassandra Project, Dynomite, HBase, Hypertable, CouchDB et
  MongoDB. <br>Le terme <b>"NoSQL"</b> a été adopté peu de temps avant
  la conférence sur le <b>cannal IRC #Cassandra</b>.<br> Cette dénomination ne
  devait initialement servir qu'à désigner cette convention<br> mais elle
  passera à la postérité en devenant la désignation de cette génération
  d'outils.
</div>

## NoSQL

--------------------------
 **N**ot **o**nly **SQL**
--------------------------

<div class="notes">
L'interprétation <b>"not only SQL"</b> ne sera inventée plus tard que comme
<b>rétro-acronyme</b>. <br>
  De nombreux spécialistes se sont plaints de l'inexactitude du
  terme "NoSQL" et des confusions qu'il pouvait créer. <br>
  Quant à son <b>inventeur
  (Carl Strozzi)</b>
  lui préférant parfois le terme <b>"NoRel" ("not only relational")</b>
  <br>ou d'autres
  désignations plus spécifiques, mais le terme reste le plus
  populaire.<br>
  L'idée sous-jacente est que le concept ne se substitue pas aux SGBD
  traditionnels <br>mais en les complétant, en comblant leurs faiblesses.
</div>

## Un peu d'humour

> Trois administrateurs de base de données entrent dans un bar NoSQL...
  Ils en ressortent un peu plus tard, sans avoir pu trouver de table...

<div class="notes">
  Cela dit, ce n'est pas tout à fait vrai, on appellera pas ca des "tables", mais plutot des "stores"
</div>

--

## NoSQL

- Gestion d'objets complexes, hétérogènes ou imbriqués
- Pas de schéma pour les données ou schéma dynamique
- Relâchent les contraintes ACID (ou même ne propose pas de gestion des
  transactions)
- Meilleur scalabilité dans des contextes fortement distribués
    - données distribuées partitionnement horizontal des données sur plusieurs
      nœuds
    - Réplication des données sur plusieurs nœuds


<div class="notes">
  Les performances restent bonnes avec la montée en charge (scalabilité) en
  multipliant simplement le nombre de serveurs,<br> solution raisonnable avec la
  baisse des coûts, en particulier si les revenus croissent en même temps que
  l'activité.<br> Les systèmes géants sont les premiers concernés : énormes
  quantités de données, structuration relationnelle faible (ou de moindre
  importance que la capacité d'accès très rapide, quitte à multiplier les
  serveurs).
  
  Un modèle typique en NoSQL est le système clé-valeur, avec une base de
  données pouvant se résumer topologiquement à un simple tableau associatif
  unidimensionnel avec des millions — voire des milliards — d'entrées. Parmi
  les applications typiques, on retrouve des analyses temps-réel, statistiques,
  du stockage de logs (journaux), etc.
</div>

---

## Exemple de contexte distribué : Data Centers

Utilisent des LAN (Local Area Networks) avec 3 niveaux de communication :

1. Serveurs en rack : Liaison réseau ~1 Go/s
2. Data Centers : Connexion inter-routeurs ~100 Mo/s
3. Entre les Data centers : 2~3 Mo/s

Les serveurs comminiquent par **envoi de messages**, ils ne partagent pas de disques, ni de ressouces de calcul :

- Architecture **shared nothing**

---

## Exemple de contexte distribué : Data Centers

### Google (2010)

- 1 Datacenter = 100~200 racks de 40 serveurs ~ 5000 serveurs
- ~ 1 000 000 de serveurs (estimation d'après consommation électrique)

### Facebook (2010)

- 2500 serveurs
- 1 PetaByte d'espace disque = 1 000 000 Gigabytes
- 250+ GigaBytes de données compressés
- 2+ Terabytes de données compressées

---

## Contextes distribués

### Système distribué

- système logiciel permettant de coordonner plusieurs ordinateurs
- ordinateurs reliés par un réseau local  (LAN)
- communiquant généralement par envoi de messages

### Architecture distribuée :

- fonctionnant sur du matériel peu spécialisé
- matériel facilement remplaçable en cas de panne

## Gestion de données distribuée

<div class="notes">
  On dispose d'un très grand ensemble de données sur lesquelles on doit leur
  appliquer des traitements
</div>

- Par **distribution des traitements** ("scaling" des traitements)
    - on **distribue ces traitements** sur un nombre de machines important afin
      d’absorber des charges très importante
    - on **envoie les données** aux endroits appropriés, et on enchaîne les
      exécutions distantes ( scénario type **Workflow** implémentable avec des **web
      services**)
- Par **distribution des données** ("scaling" des données)
    - on **distribue les données** sur un nombre important de serveurs afin de
      stocker de très grands volumes de données
    - on **"pousse" les programmes vers ces serveurs** ( plus efficace de
      transférer un petit programme sur le réseau plutôt qu'un grand volume de
      données. *Ex: algorithme* **MapReduce**

# Fondemments du NoSQL

## Fondemments du NoSQL

### Le **Sharding**

partitionnement des données sur plusieurs serveurs 

---

## Fondemments du NoSQL

### Le **Consistent hashing**

partitionnement des données sur plusieurs serveurs eux-mêmes partitionnés sur
un segment

---

## Fondemments du NoSQL

### Le **Map Reduce**

modèle de programmation parallèle permettant de paralléliser tout un ensemble
de tâches à effectuer sur un ensemble de données

---

## Fondemments du NoSQL

### Le **MVCC**

"Contrôle de Concurrence Multi-Version": mécanisme permettant d’assurer le
contrôle de concurrence

---

## Fondemments du NoSQL

### Le **Vector-Clock**

ou horloges vectorielles. Permet des mises à jours concurentes en datant les
données par des vecteurs d’horloge.

# Modèles de base données NoSQL

## Modèles de base données NoSQL

### Type **Key-Value** (Clé-valeur)

Basique, chaque objet est identifié par une clé unique constituant la seule
manière de le requêter

<div class="notes">
  Elles fonctionnent comme un grand tableau associatif et retourne une valeur
  dont elle ne connaît pas la structure
  
  -  leur modèle peut être as similé à une table de hachage (hashmap)
     distribuée
  -  les données sont simplement représentées par un couple clé/valeur
  -  la valeur peut être une simple chaîne de caractères , ou un objet
     sérialisé... 
  -  cette absence de structure ou de typage ont un impact important sur le
     requêtage : toute l’intelligence portée auparavant par les requêtes SQL
     devra être portée par l’applicatif qui interroge la BD. 
  - Implémentations les plus connues :
</div>

- Voldemort (libéré par LinkedIn)
- Redis (sponsorisé par VMWare)
- Riak (implémentation Open-Source de Amazon Dynamo)
- ...

---

## Type **Key-Value** (Clé-valeur)

\center\includegraphics[width=\textwidth]{images/key_value.png}

<div class="notes">
  Chaque objet est identifié par une clé unique seule façon de le requêter
  La structure de l’objet est libre , souvent laissé à la charge du
  développeur de l’application (XML, JSON, ...), la base ne gérant généralement
  que des chaînes d’octets
</div>

---

## Type **Key-Value** (Clé-valeur)

### CRUD

- **Create**: créer un nouvel objet avec sa clé
    - `create(key, value)` 
- **Read**: lit un objet à partire de sa clé
    - `read(key)`
- **Update**: met à jour la valeur d'un objet
    - `update(key, value)`
- **Delete**: Supprime un objet à partir de la clé
    - `delete(key)`

<div class="notes">
  - disposent générale ment d’une simple interface de requêtage HTTP REST
  accessible depuis n’import e quel langage de développement
  - ont des performances très élevées en lecture et en écriture et une scalab
  ilité horizontale considérable
  - le besoin en scalabilité verticale est faib le du fait de la simplicité des
  opérations effectuées
</div>

---

## Type **Key-Value** (Clé-valeur)

- Avantages
    - Modèle de données simple
    - Bonne mise à l'échelle horizontale pour les lecture et écritures
        - évolutivité (scalable)
        - disponibilité
        - pas de maintenances requises lors d'ajout/suppression de colonnes
- Inconvénients
    - Modèle de données TROP simple
        - Inadapté aux données complexes
        - Interrogation uniquement sur clé
        - Déporte une grange partie de la complexité de l'application sur la couche
          application elle même

---

## Type **Key-Value** (Clé-valeur)

### Utilisation principales

- dépôt de données avec besoins de requêtage très simples
- système de stockage de cache ou d’information de sessions distribuées ( quand
- l’intégrité relationnelle des données est non significative)
- les profils, préférences d’ utilisateur
- les données de panier d’achat
- les données de capteur
- les logs de données

---

## Modèles de base données NoSQL

### Type **Column** (Colonne)

Permet de disposer d'un très grand nb de valeurs sur une même ligne, de
stocker des relations "one-to-many" , d’effectuer des requêtes par clé
(adaptés au stockage de listes : messages, posts, commentaires, ...)

<div class="notes">
- Les données sont stockées par colonne , non par ligne
- on peut facilement ajouter des colonnes aux tables, par contre l'insertion
d'une ligne est plus coû teuse
- quand les données d'une colonne se ressemblent, on peut facilement
compresser la colonne
- modèle proche d’une table dans un SGBDR mais ici le nombre de colonnes
    - est dynamique
    - peut varier d’un enregistrement à un autre ce qui évite de retrouver des
      colonnes ayant des valeurs NULL. 
- Implémentatio ns les plus connues
</div>

- HBase (version Open-Souce de BigTable par Google)
- Cassandra (fondation Apache, basé sur Amazon Dynamo, né chez Facebook)
- SimpleDB (Amazon)
- Hypertable
- ...

---

## Type **Column** (Colonne)

### Principaux concepts

- Colonne

<div class="notes">
- entité de base représentant un champ de donnée
- chaque colonne est définie par un couple clé / valeur
- une colonne contenant d’autres colonnes est nommée super colonne
</div>

- Famille de colonnes

<div class="notes">
- permettent de regrouper plusieurs colonnes (ou super colonnes)
- les colonnes sont regroupées par ligne
- chaque ligne est identifiée par un identifiant unique (assimilées aux
  tables dans le modèle relationnel) et sont identifiées par un nom unique
</div>

- Super colonnes

<div class="notes">
- situées dans les familles de colonnes sont souvent utilisées comme les
  lignes d’une table de jointure dans le modèle relationnel. 
</div>

---

## Type **Column** (Colonne)

\center\includegraphics[width=\textwidth]{images/column.png}

<div class="notes">
Elles sont les plus complexes à appréhender des BD NoSQL, même si au final on
a un schéma assez proche des bases documentaires

- elles sont très utilisées pour les traitements d’analyse de données et dans
les traitements massifs (notamment via des opérations de type Map Reduce).
- elles offrent plus de flexibilité que les BD relationnelles
    - Il est possible d’ajouter une colonne ou 
    - une super colonne
    - à n’importe quelle ligne
    - d’une famille de colonnes, colonnes ou super colonne à tout instant

</div>

---

## Type **Column** (Colonne)

- Avantages
    -  Modèle de données supportant des données semi-structurées (clairsemées)
    -  Naturellement indexé (colonnes)
    -  Bonne mise à l'échelle à l'horizontale
    -  MapReduce souvent utilisé en scaling horizontal
    -  On peut voir les résultats de requêtes en temps réel
- Inconvénients
    - A éviter pour des données interconnectés
    - exige de la maintenance - lors de l'ajout/suppression/regroupement de colonnes
    - les requêtes doivent être pré-écrit, pas de requêtes ad-hoc définies
      "à la volée"

---

## Type **Column** (Colonne)

### Utilisations principales

- Netflix l'utilise notamment pour le logging et l'analyse de sa clientèle
- Ebay l'utilise pour l'optimisation de la recherche
- Adobe l'utilise pour le traitement des données structurées et de Business
  Intelligence (BI)
- Des sociétés de TV l’utilisent pour cerner leur audience et gérer le vote
  des spectateurs (nb élevé d'écritures rapides et analyse de base en temps
  réel (Cassandra)
- peuvent être de bons magasins d'analyse des données semi-structurées
- utilisé pour la journalisation des événements et pour des compteurs

---

## Modèles de base données NoSQL

### Type **Document**

Pour la gestion de collections de documents, composés chacun de champs et de
valeurs associées, valeurs pouvant être requêtées (adaptées au stockage de
profils utilisateur)

<div class="notes">
- Elles stockent une collection de "documents"
- elles sont basées sur le modèle « clé - valeur » mais la valeur est un
  document en format semi - structuré hiérarchique de type JSON ou XML
  ( possible aussi de stocker n'importe quel objet, via une sérialisation)
- les documents n'ont pas de schéma, mais une structure arborescente : ils
  contiennent une liste de champs, un champ a une valeur qui peut être une
  liste de champs, ...
- elles ont généralement une interface d’accès HTTP REST permettant d’effectuer
des requêtes (plus complexe que l’interface CRUD des BD clés/valeurs)
- Implémentations les plus connues
</div>

- CouchDB (fondation Apache)
- RavenDB (plateformes « .NET/Windows » - LINQ)
- CouchBase
- MongoDB
- Terrastore
- ...

---

## Type **Document**

\center\includegraphics[width=\textwidth]{images/document.png}

<div class="notes">
- Un document est composé de champs et des valeurs associées
- Ces valeurs :
    - peuvent être requêtées
    - sont soit d’un type simple (entier, chaine de caractère, date, ...)
    - soit elles mêmes composées de plusieurs couples clé/valeur.
- bien que les documents soient structurés, ces BD sont dites “ schemaless
  ” : il n’est pas nécessaire de définir au préalable les champs utilisés dans
  un document
- les documents peuvent être très hétérogènes au sein de la BD
- permettent d’effectuer des requêtes sur le contenu des documents/ objets
  (pas possible avec les BD clés/valeurs simples)
- Elles sont principalement utilisées dans le développement de CMS (Content
  Management System - outils de gestion de contenus).
</div>

---

## Type **Document**

- Avantages
    - modèle de données simple mais puissant (expression de structures
      imbriquées)
    - bonne mise à l'échelle (surtout si sharding pris en charge)
    - pas de maintenance de la BD requise pour ajouter/ supprimer des
      «colonnes»
    - forte expressivité de requêtage ( requêtes assez complexes sur des
      structures imbriquées)
- Inconvénients
    - inadaptée pour les données interconnectées
    - modèle de requête limitée à des clés (et indexes)
    - peut alors être lent pour les grandes requêtes (avec MapReduce)

---

## Type **Document**

### Utilisations principales

- Enregistrement d’ événements
- Systèmes de gestion de contenu
- Web analytique ou analytique temps-réel
- Catalogue de produits
- Systèmes d'exploitations

---

## Modèles de base données NoSQL

### Type **Graphe**

Pour gérer des relations multiples entre les objets (adaptés au données
issues de réseaux sociaux, ...)

<div class="notes">
- Elles permettent la modélisation , le stockage et la manipulation de données
  complexes liées par des relations non-triviales ou variables
- Modèle de représentation des données basé sur la théorie des graphes
- S’appuie sur les notions de noeuds , de relations et de propriétés qui leur
  sont rattachées. 
- Implémentations les plus connue
</div>

- Neo4j
- OrientDB (fondation Apache)
- GraphDB
- ...

---

## Type **Graphe**

\center\includegraphics[width=\textwidth]{images/graphe.png}

<div class="notes">
- Utilisent
    - un moteur de stockage pour les objets (similaire à une base documentaire,
      chaque entité de cette base étant nommée nœud)
    - un mécanisme de description d’arcs (relations entre les objets), arcs
      orientés et avec propriétés (nom, date, ...)
- Bien plus efficaces que les BDR pour traiter les problématiques
  liées aux réseaux (cartographie, r elations entre personnes, ...)
- Adaptées à la manipulation d'objets complexes organisés en réseaux:
  cartographie,réseaux sociaux , .

</div>

---

## Type **Graphe**

### Utilisations principales

Moteurs de recommandation
/ Business Intelligence (BI)
/ Semantic Web
/ Social computing
/ Données géospatiales
/ Généalogie
/ Web of things
/ Catalogue des produits
/ Sciences de la Vie et calcul scientifique (bioinformatique)
/ Données liées, données hiérarchiques
/ Services de routage, d'expédition et de géolocalisation
/ Services financiers : chaîne de financement, dépendances, gestion des risques, détection des fraudes, ... 

# NoSQL Graphe = le futur ?

<div class="notes">
Applications au WebSémantique :

- Magasins de triplets RDF ( Triple Stores) :
    - the foundation of many Semantic Web systems
    - encodé s in format/ langage RDF
    - chaque ligne a une structure «nœud-lien-noeud» (sujet-prédicat- objet)
    - possibilité de joindre des graphes ensemble automatiquement en faisant
      correspondre les identifiants des nœuds
    - possibilité de fusion automatique de 2 graphes
        - Ex : le graphe 1 a le noeud A relié à B et le graphe 2 le nœud
          B relié à C, l'union de ces graphes montre une relation de A à C.
    - l es données RDF interrogé es via le protocole/ langage de requête SPARQL
      permettant l'utilisation d'ontologies pour l'inférence (Groupe W3C RDF
      Data Access de travail)
    - Ex : Virtuoso, Sesame, Jena
</div>

<!--
  vim: spell spelllang=fr
  vim: set ft=markdown.beamer
-->
