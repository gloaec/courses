Projet Web
================
Ghislain Loaec
19 mars 2020

# Expression du besoin

Concevoir et implémenter une application Web visant à répondre aux
problématiques soulevées par les événements récents liés au COVID-19 :

  - Confinement :
      - Travail / Éducation : Dématérialisation des processus, des
        démarches et des échanges
      - Solidarités à petite échelle, valorisation du voisinage
      - Immobilité, comment lutter contre l’isolement et l’ennui
  - Santé :
      - Services médicaux saturés, personnels surchargés
      - Diagnostiques optimisables et améliorables par l’outil numérique
      - Quid des aides à domiciles, personnes en situation de handicap
      - Accès aux activités physiques et au sport
  - Économie
      - Krach boursier et conséquences de la mondialisation
      - Pénuries sur les produits importés
      - Panique, flux tendus et pénuries sur les produits de première
        nécéssité
  - Politique
      - Maintien des éléctions faute de solution numérique sécurisée et
        fiable
      - Fermeture des frontières et limitation des transports

Innovez \! Apportez des réponses techniques à des problématiques
réelles. Si votre ambition dépasse le temps imparti, travaillez en
priorité sur une preuve de concept qui vous permettera d’effectuer les
levées de fonds nécéssaires à la poursuite de votre projet.

Au vu du temps imparti, vous pouvez développer l’application de votre
choix.

## Exemples d’applications

  - Application d’entraide à la garde d’enfants, d’assistance aux
    personnes fragiles ou en perte d’autonomie (courses à l’extérieur
    pour les personnes vulnérables ou à mobilité réduite)
  - Application d’aide au diagnostique et au traitement (questionnaire
    interactif et actions à prendre en fonction de la gravité des
    symptômes)
  - Application d’aide au travail collaboratif dématérialisé (gestion
    des tâches, des calendriers, facilitation de la communication)
  - Application de vote sécurisée (permettre le vote dématérialisé,
    unique et sécurisé)
  - Application de répartition équilibrée des consomateurs (orienter des
    personnes vers une enseigne ou une autre en fonction de l’état des
    stocks, de l’affluence de gens et de la situation géographique)
  - Application de génération d’attestations de déplacement (mauvaise
    idée)

Autres exemples :

  - Module de blog / mini-CMS
  - Jeu / Quizz / Serious game interactif
  - Service d’aggrégation / d’information

# Mise en oeuvre

*Procédure à titre informatif*

  - Analyse conceptuelle du besoin
      - Présentation de la problématique
      - Objectifs de l’application (fonctionnalités, utilité)
      - Diagramme de classes (UML, MCD)
      - Justification des choix techniques (Architecture, frameworks,
        librairies)
      - Atouts et défauts du projet et limites hypothétiques
  - Mise en place d’outils de travail collaboratif
      - Moyens de communication (Channel IRC, Discord, …)
      - Dépôt de code (`git`, `svn`, `hg`)
      - Gestion des tâches (Gitlab, Github, Kanboard, …)
      - Rédaction collaborative (`git`, Framapad, …)
  - Organisation et méthodologie de travail
      - Ecriture des différents scénarios (user stories)
      - Définition des priorités, des échéances et des rôles de chacun
      - Découpage des scénarios prioritaires en sous-taĉhes de faible
        poid.
      - Implémentation et intégration continue

## Exigences particulières

  - Groupes de travail de 2 à 4 personnes
  - ~~Utilisation d’un framework (MVC ou Flux)~~
  - ~~Interface d’administration sécurisée~~
  - ~~Interface responsive~~

# Livrables

  - Un accès au dépôt de code et à l’outil de gestion des tâches
  - ~~Un document de 10 à 15 pages présentant l’analyse conceptuelle du
    besoin et les moyens mis en oeuvre.~~
  - Un fichier README.md détaillé à la racine du dépôt :
      - Présentation
      - Installation
      - Contributions
      - Versionning
      - Auteurs
      - License
  - ~~Une procédure d’installation ou une application empaquetée.~~

# Soutenance

À l’issue de la période du projet, les participants devront présenter
leur travail à l’oral :

  - 10 minutes de présentation
  - 5 minutes de démo
  - 5 minutes de questions

# Calendrier

  - 19, 20 mars 2020
      - Rappels sur la conception d’application
      - Prise de conaissance du sujet
      - Définition du besoin
      - Mise en place du travail collaboratif
  - 6 avril :
      - Travail en autonomie et aide technique de l’intervenant
  - 7 avril :
      - Soutenances orales
