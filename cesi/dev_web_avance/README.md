# Développement Web Avancé

## Cours

- [Conception d'applications](lectures/lecture.md)
  ([pdf](lectures/lecture.pdf))

## Tutos

- [Symfony (MVC)](tutorials/mvc-app.md)
  ([pdf](tutorials/mvc-app.pdf))
- [React+Redux (Flux)](tutorials/flux-app.md)
  ([pdf](tutorials/flux-app.pdf))

## Documentation

- [Javascript frameworks
  2018](documentation/js_frameworks_2018.md)
  ([pdf](documentation/js_frameworks_2018.pdf))
- Ruby-on-rails: Pile incluses
  ([pdf](documentation/ror_piles_incluses.pdf))
