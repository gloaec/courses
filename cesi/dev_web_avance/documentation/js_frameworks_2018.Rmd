---
title: Frameworks Javascript 2018
subtitle: Comparatif
author: Ghislain Loaec
date: '`r format(Sys.Date(), "%d %B %Y")`'
lang: fr-FR
output:
  html_document:
    toc: true
  pdf_document:
    toc: true
    toc_depth: 1
---

\newpage

Tableau comparatif
==================

+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
|                  | VanillaJS  | Ember       | Polymer        | Angular         | React + Redux  | Vue                  |
+==================+============+=============+================+=================+================+======================+
| Sponsor          |            | Community   | Google         | Google          | Facebook       | Community            |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Year             |            | 2011        | 2015           | 2010 (2016)     | 2013           | 2015                 |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Latest Version   |            | 2.18        | 2.0            | 5.0             | 16.2           | 2.5                  |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Language         | Javascript | Javascript  | Javascript     | Typescript      | Javascript X   | Javascript           |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Recommended tmpl |            | Handlebars  | Javascript     | Angular         | JSX            | Vue                  |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Size             | 0 KB       | 435 KB      | 127 KB         | 566 KB          | 133 KB + 2 KB  | 59 KB                |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Popularity       |            | 67.6        | 85.5           | 62.3            | 93.3           | 94.4                 |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Stars / Forks    |            | 22k+        | 21k+           | 37k+            | 95k+           | 75k+                 |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| CLI              |            | `ember cli` | `polymer cli`  | `ng cli`        | (not official) | `vue cli`            |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Architecture     |            | MVC         | MVC            | MVC             | Flux           | Flux                 |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Flexibile Arch   | Yes        | No          | No             | No              | Yes            | Yes                  |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Binding          |            | 2 way       | 2 way (def: 1) | 2 way (def: 1)  | 1 way          | 1 way                |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Logic Separation |            | `+ +`       | `+ +`          | `+ +`           | `+ + +`        | `+ +`                |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Learning Curve   |            | `- - -`     | `-`            | `- -`           | `- -`          | `-`                  |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Rendering speed  |            | `- -`       | `-`            | `-`             | `+ +`          | `+ +`                |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Mobile build     |            | Cordova     |                | Ionic           | React-Native   | Weex                 |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Testing          |            | Q-Unite     | Jasmine        | Jasmine + Karma | Any            | Karma + Mocha + Jest |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Data Library     |            | Ember-data  |                |                 | Redux          |                      |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Documentation    |            | `+ +`       | `+ +`          | `+ +`           | `+`            | `+ +`                |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Community        |            | `- -`       | `-`            | `+ +`           | `+ +`          | `+`                  |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| API Stability    |            | `+ + +`     | `+`            | `-`             | `+`            | `+ + +`              |
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+
| Best attraction  | A la carte | Principals  | Webcomponents  | Oldest          | Virtual        | Combination          |
|                  |            | first       | W3C            | Framework       | DOM            | Angular + React      | 
+------------------+------------+-------------+----------------+-----------------+----------------+----------------------+

Autres frameworks populaires: Meteor, Riot, KnockoutJS, Inferno, Aurelia, BackboneJS, ExtJS/Sencha.

Comparaison technique
=====================

Angular
-------

L'utilisation de Typescript est probablement une bonne idee : beaucoup de
fonctionnalités sont implémentées dans Typescript avant même les spécification
Ecmascript. Cela garantit une certaine modernité de l'application et un marge
d'erreur plus faible avec un language fortement typé.

React
-----

Très appréciable pour ses performances. Le virtual DOM est la killer feature et
les autres frameworks tendent vers cette approche (Vue, Angular). Avec
l'exemple de `react-fiber`, on ne peut qu'admirer la réactivité des
head-developpers quand des problemes se manifestent. Facebook étant le lieu le
plus fréquenté d'Internet aujourd'hui, on ne peut que espérer que la team
continue dans leur lancée avec de plus en plus de technologies avant gardistes.

Ember
-----

`Ember-data` est une librairie de gestion de données (pas seulement pour
Ember), qui facilite drastiquement le requêtage et la gestion du cache. Il
dissipent tous les maux de têtes induits par la recupération, la manipulation,
le stockage et le cache de données coté client. Un atout réside également dans
le fait qu'il oblige à respecter des bonnes pratiques imposées (une seule
manière de faire). Recommandé pour les débutants, et permet une bonne
introduction vers des architectures plus modernes.

Vue
---

Vue est un framwork hybrid Angular/React. Angular est volumineux et supporte
majoritairement Typescript ; tandis que le JSX de React n'est a adapté pour
tous les scénarios. Vue est alors la solution : Vue revendique etre le
framework le plus rapide en ce qui concerne la manipulation du DOM.

Polymer
-------

Polymer évolue en concordance directe avec les standards W3C. Il propose
également une approche orientée composants, toutefois un tantinet plus verbeuse
que ses conccurrents. Elle ne propose pas de librairie pour la manipulation de
données.


Performances
============

Angular
-------

Angular souffre des serieux soucis de performance depuis la version 2. La
vesion 5 d'Angular est sortie et il semblerait que les choses se soit
grandement améliorées, mais encore insuffisant comparé à ses conccurents. Le
fait que Angular soit porté par Google offre une certaine confiance quant à la
résolution imminente de ces problèmes.

React
-----

Les concepts de virtual dom et de data-binding à sens unique font de React le
plus performant du marché actuel. il arrive fréquemment que, dans des projets
Ember ou Angular, les développeur ait recours à React pour des pages contenant
une grande quantité d'éléments.

Ember
-----

C'est un éléphant un peu lent, particulièrement dans le rendu. Les observeurs
et les détecteurs de changement sont plus rapides qu'Angular, mais le premier
rendu est super lent.

Vue
---

Vue partage beaucoup de concepts techniques avec React : Virtual DOM,
composants réactifs, ... Les performances sont similairement excellentes.
Toutefois, Vue pretend être plus rapide dans la detection de changement du DOM.

Polymer
-------

Polymer affiche des performances moyennes d'une manière générale, plus
particulièrement en phase de développement. En production, la minification
solutionne en partie le problème. Le premier rendu est très couteux, mais une
fois en mémoire, les performances restes correctes.


Architecture de code
====================

Angular
-------

Le framework MVC a fourni suffisament d'effort dans la CLI pour proposer une
architecture de projet prédéfinie. C'est plus facile de démarrer rapidement,
mais plus difficile pour tendre vers une architecture distribuée.

React
-----

React n'a pas d'architecture à proprement parler, mais le communauté utilise
Flux. Flux incarne définitivement une meilleure architecture que MVC, mais la
phase de configuration peut être très douloureuse pour les débutants. La
principale raison provient du fait que  React s'efforce à demeurer
non-opiniâtre et flexible.

Ember
-----

Un des meilleurs distributeurs de logique. Le projet créé via la CLI est très
riche et l'utilisation de Ember-data facilite vraiment la vie. Tous les
développeurs Ember tombent amoureux de l'architecture par défaut.

Vue
---

Très similaire à Angular. Facilement distribuable et structurable,
l'empaquetage est très permissif. Le problème d'architecture le plus notable
avec Vue, c'est l'absence totale de notion d'héritage entre composants :
obligation d'utiliser des mixins.

Polymer
-------

L'arborescence par défaut est assez simple et non conventionée. Polymer
laisse beaucoup de liberté dans l'organisation des fichiers d'un projet, mais
la phase d'empaquetage avec Vulcanize promet des scéances intenses d'arrachage
de cheveux.


CLI
===

Angular
-------

Pas obligatoire, mais l'outil est arrivé quelques jours après la naissance du
framework, déjà assez puissant, et il s'améliore de jours en jours.

React
-----

React n'as pas de CLI officiel, mais la communauté utilise beaucoup celui ci :
<https://github.com/facebookincubator/create-react-app>

Ember
-----

Ember-cli existe déjà depuis un moment et a largement contribué à la
démocratisation des CLI dans les frameworks JS. `Angular-cli` est un fork
`ember-cli`.

Vue
---

La CLI Vue est pas mal : Il permet de démarrer un projet avec un template de
notre choix : webpack, pwa, browserify, et d'autres choses. L'outil de
génération de projet est bien mais il ne fait pas grand chose d'autre.

Polymer
-------

La CLI permet de créer un projet, tester, servir l'application et builder.
Néanmoins, elle manque cruellement d'un générateur de code.


Courbe d'apprentissage
======================

Angular
-------

C'est un framework, donc de toute évidence, il doit y avoir apprentissage.
Angular implemente son propre language pour la gestion des templates, les
boucles et autres. La courbe d'apprentissage est donc relativement importante.

React
-----

React, c'est du Javascript. Un librairie simple avec quelques apis et un
concept de flux de données certes un peu difficile à assimiler. Sinon, en
partant du postulat qu'on connait Javascript, l'effort reste modéré.

Ember
-----

Ember est également un framework. Donc, comme Angular, la courbe
d'apprentissage est importante. Sachant que Ember favorise la convention par
rapport à la configuration, assimiler toutes les bonnes pratiques peut prendre
un certain temps. D'un autre coté, ces mécanismes clairement définis et imposés
garantissent l'harmonie du code.

Vue
---

Vue semble plus facile à prendre en main que Angular et recense quelques
concepts supplémentaires comparé à React. N'importe qui ayant des connaissances
sur Angular et React réussiront à adopter très facilement le produit.

Polymer
-------

La courbe d'apprentissage est minimisée par l'utilisation des standards W3C et
par les connaissances préalables de l'équipe sur cette techno.


Documentation
=============

Angular
-------

Bonne documentation, courte, simple et droit au but. Les documentations
offcielles sont souvent suffisantes. En outre et au delà du fait que la
communauté Angular soit très large, AngularJS est un des frameworks avec le
plus de bouteille, donc il est très facile de trouver de l'aide sur de
nombreux blog/forums.

React
-----

React est tellement flexible que les gens l'utilise comme ils le souhaitent.
C'est pourquoi, c'est parfois difficile de trouver de la documentation de bonne
qualité. Néanmoins, Internet est tellement riche qu'on trouvera toujours
réponse aux questions convenablement posées. La doc officielle est un peu
légère et n'est vraisemblablement pas adaptée pour les débutants.

Ember
-----

À ses débuts, Ember souffrait d'une documentation très pauvre, le problème ne
semble néanmoins plus du tout d'actualité : la documentation est très bien
organisée, détaille le framework en profondeur et chaque concept est accompagné
d'un morceau de code. Toutefois, en raison d'une communauté réduite, il est
difficile de trouver des ressources sur les blog/forums. VoidCancas est l'un
des rares blogs à proposer des contenus Ember dans une large mesure.

Vue
---

Les tutoriaux sont inutiles à partir du moment qu'on a déja une expérience avec
Angular, les guides officiels sont suffisants. La communauté s'est largement
enrichie depuis quelques mois et met à disposition de plus en plus de
tutoriaux.

Polymer
-------

La couche Polymer agit comme un polyfill pour l'utilisation des Webcomposants
standards. Ces derniers sont normés et la documentation est en directe
adéquation avec les spécifications W3C. La documentation officiel est correcte,
mais peu de forums/blogs s'y intéresse.


Support des organisations
=========================

Angular
-------

Concoqueté par le puissant Google, il est porté une équipe de développeurs
dévoués. Cette techno commence à être utilisée dans plusieurs projets au sein
de Google, ce qui n'était pas le cas auparavant et posait de nombreuses
questions quant à la confiance à accorder au produit. Très controversé à deux
reprises :

- Passage de AngularJS à Angular 2 : Aucune rétrocompatibilité, probablement
  dûe à l'immaturité du produit au moment de sa première publication.
- Abandon volontaire de la version 3 : Problème technique mineur induit par le
  versionnage sémantique lié à l'utilisation de MonoRep. Angular 2 dépendait de
  la librairie `@Angular/router` en sa version 3, les développeurs ont
  volontairement choisi de repartir sur des version cohérentes entre elles afin
  d'éviter toute confusion.

React
-----

Facebook a créé le produit pour ses propres besoins et a libéré le code par la
suite. Étant donné que Facebook est la plateforme la plus fréquentée au monde,
elle requiert une performance à toute épreuve. Nous sommes de facto assurés
qu'aucune release de React ne compromettera la performance du produit.

Ember
-----

Indépendant de toute organisation, mais LinkedIn est 100% Ember et Microsoft,
Apple, Netflix et beaucoup d'autres boites l'utilisent aujourd'hui.

Vue
---

Encore une fois indépendant. Le plus grand utilisateur aujourd'hui est Alibaba.

Polymer
-------

Meme argument que pour Angular, à la différence que de nombreux projets Google
s'appuient sur cette techno :
<https://github.com/Polymer/polymer/wiki/Who's-using-Polymer>


Support de la communauté
========================

Angular
-------

C'est le framework le plus vieux, ainsi la commauté est gigantesque. Angular
incarne un des premiers acteurs à faire évoluer le framework Javascript d'un
point de vue conceptuel. Néanmoins, suite à l'échec de la version 2, il a perdu
énormément en popularité. Espérons que les efforts de la lead-team renverseront
la vapeur et replacera le produit au rang des maîtres du domaine.

React
-----

React semble rencenser aujourd'hui la plus grande part d'adhérents dans le
communauté Javascript actuelle. On trouve sur Internet une quantité
indénombrable de plugins, librairies, sdks et blogs.

Ember
-----

Ember n'a jamais eu une grande communauté, vraisemblablement en raison d'une
courbe d'apprentissage très verticale et des performances très moyennes. De
nombreux adhérents dévoués investissent dans le produit en développant des
plugins, mais la bibliothèque n'est en rien comparable à celles de géants.

Vue
---

La communauté Vue a grandie de manière exponentielle ces derniers mois. Bien
qu'une grande partie ne parle pas Anglais, cela n'impute en aucun cas la
quantité de contributions consultables.

Polymer
-------

Assez paradoxalement le framework le moins populaire parmis tous
les candidats alors qu'il est le seul à vraiment évoluer avec les standards
des Webcomponents. Deux explications possibles :

- La team ne fait aucun effort sur la publicité du produit par pur manque
  d'énergie.
- Impossible pour eux de pousser un produit alors qu'un autre framework en
  interne (Angular) est mis en avant.


Préférences de langages
=======================

Angular
-------

Angular affirme qu'il est possible de développer une application à l'aide de
Javascript natif ... mais non. Cela implique de nombreuses complications qui
vous résoudra inéluctablement à utiliser Typescript. Il n'y  pas d'alternative
à Typescript avec Angular.

React
-----

Bien que JSX ne ressemble pas à du javascript, il suffit de retirer le code
html et c'est du Javascript tout ce qu'il y a de plus standard. Il est donc
inutile d'apprendre un nouveau langage, a contrario, JSX est le seul langage
possible pour React.

Ember
-----

Ember est pur Javascript. On peut construire un projet quasi entièrement
à l'aide de la CLI et on peut utiliser ES6+. L'utilisation de Handlebars nous
empêche d'implémenter quelquonque logique dans les templates, ce qui est une
bonne chose.

Vue
---

Le cas de Vue est similaire à celui de React. Bien que l'extension soit
".vue", c'est ni plus ni moins qu'une combinaison de Javascript, HTML et CSS.

Polymer
-------

Javascript, HTML, CSS, rien de plus standard.


Développement mobile
====================

Angular
-------

La communauté utilise le framework Ionic pour développer des applications
mutliplateformes avec Angular. Peu de code natif, mais beaucoup de composants
dans le catalogue.

React
-----

React a un fort avantage dans ce domaine. React-Native est une manière simple de
composer des application natives, en implémentant un système de transpilage
permettant d'obtenir du code legacy pour chaque constructeur (Android, Apple,
Microsoft (?)). Melleures perfs dans la globalité, mais catalogue de composant
plus limité.

Ember
-----

Avec Cordova, il est possible de convertir une application Ember en application
mobile.

Vue
---

La communauté Vue ne semble pas encore très décidée sur ce point, mais beaucoup utilisent Weex.

Polymer
-------

Il est tout à fait possible d'envisager la publication d'une application
Polymer avec Cordova. La documentation est toutefois limitée et malgré un large
choix de Webcomponents dans le catalogue, aucun ne tire potentiellement
avantage du contexte mobile, ni des fonctionnalités natives.

