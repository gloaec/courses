Conception d’applications
================
Ghislain Loaec
18 mars 2020

# Introduction

## Plan

\columnsbegin
\column{.7\textwidth}
  \tableofcontents
\column{.3\textwidth}

![Application](images/application.png)  

# Bonne pratiques

## Choix technologiques

1.  **Nécéssité** de mise en place (YAGNI, KISS…)
2.  **Zone de Confort** vs **Territoire Inconnu**
      - Temps/motivation pour appréhender un nouvelle techno
3.  **Pérennité** de la solution
      - Maintenabilité du produit
      - Ancienneté du produit
      - Poids de la communauté
      - Stabilité du produit
4.  **Performance**

## Choix d’architecture

  - Réactivité
      - Dynamisme de l’application
  - Scalabilité
      - Volumétrie de données/utilisateurs
  - Portabilité
      - Terminaux compatibles

## Gestion des versions

  - `git`
  - Semantic versionning 2.0.0 (`git-flow`)
      - `MAJOR.MINOR.PATCH`
          - `MAJOR` Version pour changements d’API incompatibles.
          - `MINOR` Version pour ajouts de fonctionnalités
            rétro-compatibles
          - `PATCH` Version pour résolutions rétro-actives de bugs

## Intégration continue

Permet de s’assurer :

  - de la **qualité d’une application**. On peut ainsi s’apercevoir au
    plus tôt d’erreurs intégration, suite à un oubli d’inclusion par
    exemple, ou des régressions possibles.
  - du **respect des normes de nommage**, de programmation… établies
    pour l’application.

-----

## Tests

### Test unitaire

Vérifie qu’un composant, une méthode ou une fonction de l’application se
comporte correctement.

Ex\*: La fonction `getName()` doit retourner le nom d’un produit.

### Test fonctionnel

Vérifie de bout en bout qu’une fonctionnalité a été implémentée telle
que spécifiée.

Ex\*: Ajouter un nouveau produit au catalogue

## Méthodologie de gestion de projet

  - Watefall
  - V
  - Spiral
  - Agile
  - SCRUM
  - Extreme Programming (XP)

## Outils de gestion de projet

  - Github/Gogs
  - Redmine/OpenProject
  - Gitlab
  - Tuleap

# Architectures monolithiques

## Définition

### Monolithique

…qui forme un bloc, un ensemble rigide, homogène, impénétrable.

### Monolithique *(programmation informatique)*

…qui n’est pas scindé en plusieurs couches, modules ou services.

## Paradigme MVC - Définition

### **M**odel

Gestion de la couche de données

### **V**iew

Gestion de la couche interface utilisateur

### **C**ontroller

Gestion des interactions entre le modèle et la vue

## Paradigme MVC - Schéma

![MVC](images/mvc.png) 

-----

![MVC](images/mvccomponents.png) 

## Base de données SQL

  - `mysql`
  - `postgresql`
  - `oracle`
  - `sqlite` (dev uniquement)

## SQL: Select

Tous les utilisateurs :

``` sql
SELECT * FROM users
```

Tous les administrateurs :

``` sql
SELECT * FROM users WHERE role="admin"
```

Utilisateur avec l’identifiant `1` :

``` sql
SELECT * FROM users WHERE id=1 LIMIT 1
```

## PHP: Select

Tous les produits :

``` php
$product_id = $_GET['product_id'];
$query      = "SELECT * FROM products "
            . "WHERE id='$product_id'");
$results    = mysql_query($query);
```

## WOW: Security Alert \!

``` php
$_GET['product_id'] = " ' UNION
  SELECT username, password
  FROM users
";
```

Toujours traiter les données entrantes :

``` php
$product_id = $_GET['product_id'];
$query      = "SELECT * FROM products "
            . "WHERE id='"
            . mysql_real_escape_string($product_id)
            . "'");
$results    = mysql_query($query);
```

## ORM

  - `php` -\> `Doctrine` (Symfony), `CakePHP`, `Laravel`
  - `ruby` -\> `ActiveRecord` (RoR), `DataMapper`
  - `python` -\> `SQLAlchemy`, `Peewee`, `Django`
  - `node` -\> `Squeletize`, `(Mongoose)`

# Symfony

## ORM: Select

Afficher le nom de tous les utilisateurs :

``` php
$users = Doctrine::getTable('users')->findAll();
foreach($users as $user)
  echo "<h2>$user->username</h2>";
```

-----

Récupérer le nom d’une categorie :

``` php
function getCategoryName($category_id) {
  $q = Doctrine_Query::CREATE()
  ->select('name')
  ->from('Category c')
  ->where('c.category_id = ?',$category_id);
  return $q->fetchOne();
}
```

-----

Compter le nombre de numéros de téléphones pour chaque utilisateur :

``` php
$q = Doctrine_Query::create()
    ->select('u.*, COUNT(DISTINCT p.id) AS nb')
    ->from('User u')
    ->leftJoin('u.Phonenumbers p')
    ->groupBy('u.id');
$users = $q->fetchArray();

echo $users[0]['Phonenumbers'][0]['nb'];
```

## Model

`$ php bin/console make:entity Product`

`src/Entity/Product.php`

\scriptsize

``` php
use Doctrine\ORM\Mapping as ORM;

class Product {
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=100)
   */
  private $name;
}
```

## Model: Getters / Setters

\scriptsize

``` shell
$ php bin/console doctrine:generate:entities     # symfony < 4
```

`src/Entity/Product`

\scriptsize

``` php
class Product {
  ...

  public function getId() {
      return $this->id;
  }

  public function getName() {
      return $this->name;
  }

  public function setName($name) {
      $this->name = $name;
  }
}
```

## Model: Migrations

\scriptsize

``` shell
$ php bin/console doctrine:migrations:diff
$ cat /path/to/project/doctrine/src/Migrations/Version20171122151511.php

[...]

class Version20180104140143 extends AbstractMigration {

  public function up(Schema $schema) {
      $this->addSql('CREATE TABLE `Product` (...)');
  }

  public function down(Schema $schema) {
      $this->addSql('DROP TABLE `Product`');
  }
}

$ php bin/console doctrine:migrations:migrate
```

## Router \<\> Controller

\scriptsize

``` shell
$ php bin/console generate:controller \
  --actions="showAction:/products/{slug} listAction:/products/"
```

`src/Controller/ProductController.php`

\scriptsize

``` php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends Controller
{
  /**
   * @Route("/products", name="product_list")
   */
  public function list() {}

  /**
   * @Route("/products/{slug}", name="product_show")
   */
  public function show($slug) {}
}
```

## Controller \<\> Renderer

`src/Controller/ProductController.php`

\scriptsize

``` php
class BlogController extends Controller {

  /** @Route("/products", name="product_list") */
  public function list() {
    return new Response('<html><body>
      Product list
    </body></html>');
  }

  /** @Route("/products/{slug}", name="product_show") */
  public function show($slug) {
    return new Response('<html><body>
      Product Ref: '.$slug.'
    </body></html>');
  }
}
```

## Renderer \<\> Templates

Le language PHP est verbeux et d’autant plus lorsqu’il s’agit d’échapper
des caractères :

\scriptsize

``` php
<?php echo $var ?>
<?php echo htmlspecialchars($var, ENT_QUOTES, 'UTF-8') ?>
```

Le template permet de réduire la verbosité et propose un approche
lisible pour le rendu :

\scriptsize

``` html
{{ var }}
{{ var|escape }}
{{ var|e }}         {# shortcut to escape a variable #}
```

## Router \<\> Controller \<\> Model \<\> Renderer

`src/Controller/ProductController.php`

\scriptsize

``` php
/** @Route("/products", name="product_list") */
public function list() {
  $products = $em->getRepository('AppBundle:Product')->findAll();
  return $this->render('product/list.html.twig', array(
    'products' => $products
  ));
}

/** @Route("/products/{slug}", name="product_show") */
public function show($slug) {
  $product = $em->getRepository('AppBundle:Product')->findOneBy(array(
    'slug' => $slug
  ));
  return $this->render('product/show.html.twig', array(
    'product' => $product
  ));
}
```

## Template

`src/Resources/views/product/list.html.twig`

\scriptsize

``` html
<h1>Products</h1>
<ul>
  {% for product in products: %}
    <li>
      <a href="{{ url_for('product_show', product.slug) }}">
        {{ product.name }}
      </a>
    </li>
  {% endfor %}
</ul>
```

`src/Resources/views/product/show.html.twig`

\scriptsize

``` html
<h1>Product : {{ product.name }}</h1>
<p>Price : {{ product.price|some_currency_filter }}</p>
<a href="{{ url_for('product_list') }}">Back to list</a>
```

## I  Symfony

## Frameworks MVC

  - Serveur
      - PHP: Symfony / Silex / CakePHP
      - Ruby: Rails / Sinatra
      - Python: Django / Flask
      - Node: Sails
      - Java: Spring / GWT
  - Client (Javascript)
      - Ember
      - Backbone
      - Sencha Ext JS

# Architectures composants

## Paradigme MVVM (Variante MVC)

![MVC](images/mvvm.png) 

## Paradigme MVVM (Variante MVC)

![MVC](images/mvvm1.png) 

# Angular

## Composant Angular

\scriptsize

``` javascript
@Component({ selector: 'custom-counter', ... })
export class CustomCounterComponent {
  counterValue = 0;

  get counter(){ return this.counterValue; }
  set counter(value) { this.counterValue = value; }
  decrement() { this.counter--; }
  increment() { this.counter++; }
}
```

\scriptsize

``` html
<button (click)="decrement()">-</button>
<span>{{counter}}</span>
<button (click)="increment()">+</button>
```

Utilisation :

\scriptsize

``` html
<custom-counter></custom-counter>
```

## Simple data-binding \<\> One-way binding

\scriptsize

``` javascript
@Component({ selector: 'custom-counter', ... })
export class CustomCounterComponent {

  counterValue = 0;

  @Input()
  get counter(){
    return this.counterValue;
  }
  ...
}
```

Utilisation :

\scriptsize

``` html
<custom-counter [counter]="someValue"></custom-counter>
```

## Double data-binding \<\> Two-ways binding

\scriptsize

``` javascript
@Component({ selector: 'custom-counter', ... })
export class CustomCounterComponent {

  ...
  @Output() counterChange = new EventEmitter();

  set counter(val) {
    this.counterValue = val;
    this.counterChange.emit(this.counterValue);
  }
  ...
}
```

Utilisation :

\scriptsize

``` html
<custom-counter [(counter)]="someValue"></custom-counter>

<p>counterValue = {{someValue}}</p>
```

## 

\begin{flushright}
\vskip 2cm
\LARGE I \ensuremath{\heartsuit} Angular
\end{flushright}

## Frameworks MVVM

  - Client (Javascript)
      - AngularJS -\> Angular
      - Polymer
      - Vue

# Architectures distribuables

## Naissance

Le **big data** désigne des ensembles de données devenus si
**volumineux** qu’ils dépassent l’**intuition**, les capacités humaines
d’analyse et même celles des outils informatiques **classiques** de
gestion de base de données ou de l’information

## Paradigme Flux

![MVC](images/flux.png) 

-----

![MVC](images/fluxcomponents.png) 

## Base de données NoSQL

|                          |
| :----------------------: |
| **N**ot **o**nly **SQL** |

  - Google: **Bigtable** (2005)
  - Amazon: **Dynamo** (2004-2007)
  - Ubuntu One: **CouchDB** (2005)
  - Facebook: **Cassandra** (2008) puis **HBase**
  - LinkedIn: **Project Voldemort** (2009)
  - Sourceforge.net: **MongoDB** (2009)
  - …

## NoSQL: Map-Reduce

![MVC](images/mapreduce.png) 

## NoSQL: Map-Reduce

\scriptsize

``` javascript
var db = new PouchDB('movies');

db.bulkDocs([
  {"_id": "1", "title": "Rambo",        "genre": "Action"},
  {"_id": "2", "title": "Forrest Gump", "genre": "Drama"},
  {"_id": "3", "title": "Gladiator",    "genre": "Action"},
  {"_id": "4", "title": "The Mask",     "genre": "Comedy"}
]);
```

-----

\scriptsize

``` javascript
db.then(function () {

  var map = function(doc) {
    emit(doc.genre, 1);
  };

  var reduce = function(keys, values, rereduce) {
    return count(values);
  };

  db.query(
    { map: map, reduce: reduce },
    { reduce: true, group: true, group_level: 1 },
    function(err, response) {
      console.log(JSON.stringify(response.rows));
    }
  );
});
```

-----

Résultat :

``` javascript
[
  { "key": "Action", "value": 2 },
  { "key": "Comedy", "value": 1 },
  { "key": "Drama",  "value": 1 }
]
```

## Reduce / Rereduce

![MVC](images/rereduce.png) 

## Reduce / Rereduce

![MVC](images/rereduce1.png) 

# ReactJS

## Structure d’un composant

`components/HelloWorld.js`

\scriptsize

``` javascript
import React from 'react'

class HelloWorld extends React.Component {

  render() {
    return (
      <div className='hello-world'>
        <h1>Hello World</h1>
      </div>
    )
  }

}

export default HelloWorld
```

## Props (One-way binding)

`components/HelloWorld.js`

\scriptsize

``` javascript
import React from 'react'

class HelloWorld extends React.Component {

  render() {
    return (
      <span>{ this.props.text }</span>
    )
  }
}

export default HelloWorld
```

## 

`App.js`

\scriptsize

``` javascript
import React from 'react'
import ReactDOM from 'react-dom'
import HelloWorld from './components/HelloWorld'

const mountpoint = document.getElementById('props-example');

class App extends React.Component {

  render() {
    return (
      <HelloWorld text="foo bar" />
    )
  }

}

ReactDOM.render(<App />, mountpoint)
```

## State

`components/Clock.js`

\scriptsize

``` javascript
import React from 'react'

class Clock extends React.Component {

  constructor(props) {
    super(props)
    this.state = { time: new Date() }
    setInterval(this.tick.bind(this), 1000);
  }

  render() { return <div>Time: { this.state.time.toString() }</div> }
  tick()   { this.setState({ time: new Date() }); }

}

export default Clock
```

## Évènements du DOM

`components/Counter.js`

\scriptsize

``` javascript
import React from 'react'

class Counter extends React.Component {

  state = { count: 0 }

  render() {
    return <div>
      <button onClick={ () => this.increment() }>+1</button>
      Count: <span>{ this.state.count }</span>
      <button onClick={ () => this.decrement() }>-1</button>
    </div>
  }

  increment() { this.setState({ count: this.state.count + 1 }) }
  decrement() { this.setState({ count: this.state.count - 1 }) }
}

export default Counter
```

## Cycle de vie d’un composant

`components/ClockLifeCycle.js`

\scriptsize

``` javascript
import React from 'react'

class ClockLifeCycle extends React.Component {

  this.state = { time: new Date() }

  componentDidMount() {
    this.intvl = setInterval(this.tick.bind(this), 1000);
  }
  componentWillUnmount() {
    clearInterval(this.intvl);
  }

  render() { return <div>Time: { this.state.time.toString() }</div> }
  tick()   { this.setState({ time: new Date() }); }
}

export default Clock
```

## I  React

# Conclusion

<!--
  vim: spell spelllang=fr
-->
