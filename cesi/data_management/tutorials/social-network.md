Gestion des données relationnelles
================
Ghislain Loaec
08 novembre 2020

# Introduction

\begin{wrapfigure}{l}{0.40\textwidth}
  \begin{center}
    \includegraphics[width=0.35\textwidth]{images/gtd.jpg}
  \end{center}
\end{wrapfigure}

Vous êtes Mark Ranlargent, étudiant quarterback très populaire à
l’univesité de Okésitérich. Vous enchaînez les soirées au club
\(\chi\pi Q\nu\), les farine partys, jusqu’au jour où vous commencez à
développer sérieuse allergie au gluten.

Lors de votre sevrage, vous réalisez qu’il vous est impossible de
poursuivre votre carrière de footballeur professionnel avec un tel
handicap. Vos amis Eduardo Sérarien, Dustin Ragequitz et Chris Oof sont
là pour vous soutenir et vous mettent dans l’intimité pour un projet sur
lequel il travaillent. Vous identifiez tout de suite le potentiel de
l’application et vous décidez de commencer votre carrière dans le
numérique.

Vous et vos amis ne le savent pas encore, mais vous êtes les fondateurs
du nouveau produit des internets qui va revolutionner à tout jamais la
communication et les interractions humaines : **GTD**

# Analyse conceptuelle

## Regles de gestion 1.0

  - L’application comporte des utilisateurs
  - Le utilisateurs doivent pouvoir s’authentifier sur la plateforme
    avec identifiant et mot de passe (obligatoires)
  - Il y 3 types d’utilisateurs :
      - créateurs de contenu
      - modétateurs
      - administrateurs
  - Chaque utilisateur peut remplir son profil avec les informations
    suivantes :
      - Pokémon préféré
      - Genre (LGBTQXYZKOARPF)
  - Tous les utilisateurs peuvent créer des posts
  - Les posts comporte une description
  - Les utilisateurs peuvent demander d’autres utilisateurs en ami
  - La demande d’amitié doit être acceptée avant d’être effective
  - Les amis peuvent commenter les publications de leur amis
  - Seul l’auteur et ses amis peuvent consulter un post et ses
    commentaires

## Diagramme des classes

Modélisez l’application et ses règles de gestions avec des **entités**
et des **associations**

## Choix technologiques

  - **Application** : Langage au choix, framework MVC recommandé, ORM
    fortement recommandé
      - *Php* : Symfony, Silex, Laravel, CakePHP
      - *Python* : Django, Flask
      - *Ruby* : Rails, Sinatra
      - *Javascript* : Sails, Express/Sequelize
  - **Base de données** : Vous êtes en l’an 2002, SQL et pis c’est tout.
      - *SQL* : PostgreSQL, MySQL/MariaDB, SQLite (dev only)
  - **Gestion de projet** : Git obligatoire, Board facultatif
      - *Dépôt de code* : Gitlab, Github, Bitbucket (ou self-hosted)
      - *Gestion des tâches* : Gilab, Github, Trello, Tuleap
  - **Canaux de communications** :
      - *Email* : Adresses CESI (géré par le DPO CESI)
      - *Chat* : Discord canaux assignés (Saas)
      - *Videoconférence* : Discord canaux assignés (Saas)

# Implémentation *Express/Sequelize*

## Structure du projet

Installer `npm` (ou `yarn`)

``` bash
TODO
```

Installer `express-generator`

``` bash
sudo npm install express-generator -g
```

Créez le squelette de votre application `gtd`

``` bash
express gtd -e -v ejs -c sass --git
```

Accèdez au repertoire de l’application

``` bash
cd gtd
```

Initialisez le dépôt `git`

``` bash
git init
```

Renseignez soigneusement les informations de votre projet

``` bash
npm init
```

Installez express et les dépendances de votre
projet

``` bash
npm install express ejs body-parser express-validator express-sessions --save
```

## Base de données

Installons l’ORM sequelize pour la mnipulation des données et la gestion
des migrations. Le projet étant toujours à l’étape de prototype, nous
utiliserons pour l’instant sqlite comme moteur de base de donnée,
suffisant pour nous besoins actuels.

``` bash
npm install sequelize sequelize-auto sequelize-cli sqlite3 --save
```

Comme `sequelize-cli` et `sequelize-auto` seront installées dans le même
dossier de l’application, on utilisera npx : ( un utilitaire qui permet
d’exécuter des binaires à partir de paquets npm).

``` bash
npx sequelize-cli init
```

Cette commande crée 4 dossiers:

  - `models`
      - `models/index.js`
  - `config`
      - `config/config.json`
  - `migrations`
  - `seeders`

Changez la configuration de votre environnement de développement :

`config/config.json`

``` json
"development": {
  "username": "root",
  "password": null,
  "database": "gtd_development",
  "host": "127.0.0.1",
  "dialect": "sqlite",
  "storage": "gtd.sqlite"
},
```

Dans notre exemple, sequelize utilisera les informations de connexion
contenues dans “developpement”. On peut savoir cela grâce à la ligne
suivante dans le fichier models/index.js

``` js
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
```

### Utilisateurs

Utilisons la commande suivante pour créer un nouveau model
`User`

``` bash
npx sequelize-cli model:generate --name User --attributes username:string,password:string
```

Modifions ensuite notre fichier de migration pour préciser certaines
subtilité sur l’intégrité de données :

``` js
username: {
  type: Sequelize.STRING,
  allowNull: false
},
password: {
  type: Sequelize.STRING,
  allowNull: false
},
role: {
  type: Sequelize.STRING,
  allowNull: false,
  defaultValue: 'user'
},
createdAt: {
  allowNull: false,
  type: Sequelize.DATE,
  defaultValue: new Date()
},
updatedAt: {
  allowNull: false,
  type: Sequelize.DATE,
  defaultValue: new Date()
}
```

Nous pouvons alors lancer la migration du schéma de notre base de
données :

``` bash
npx sequelize-cli db:migrate
```

Nous avons la possibilité de ensuite d’importer des données initiales
dans notre application. C’est ce qu’on appelle les `seeds` (ou
`fixtures` qui se rapporte plutôt aux tests)

Créons une `seed` pour les administrateurs pour la plateforme :

``` bash
npx sequelize-cli seed:generate --name user-admin
```

`seeders/2020XXXXXXX-user-admin.js`

``` js
'use strict';

const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
     await queryInterface.bulkInsert('Users', [{
       username: 'admin',
       password: bcrypt.hashSync('admin', 10),
       role: 'admin'
     }], {});
  },

  down: async (queryInterface, Sequelize) => {
     await queryInterface.bulkDelete('Users', null, {});
  }
};
```

Nous pouvons ensuite populer la base de données :

``` bash
npx sequelize-cli db:seed:all
```

**Attention** les données seront insérées autant de fois que la commande
sera lancée.

Vérifiez en SQL que les données sont bien présentes en base :

``` bash
npx sqlite3 gtd.sqlite
```

``` sql
SELECT * FROM users;
```

## Authentification

Créons ensuite le modèle qui nous permettera de stocker les jeton
d’authentification

``` bash
npx sequelize-cli model:generate --name AuthToken --attributes token:string userId:integer
```

Modifiez le fichier de migration pour interdire la valeur null pour le
jeton et préciser la clé étrangère vers l’utilisateur :

``` js
  token: {
    type: Sequelize.STRING,
    allowNull: false
  },
  userId: {
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
    allowNull: false
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: new Date()
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: new Date()
  }
```

Migrons le nouveau schéma de la base de données :

``` bash
npx sequelize-cli db:migrate
```

### Associations

Précisons à l’ORM que les entités `User` et `AuthToken` sont liées par
une association de type `hasMany` profitons en pour créer les méthodes
liées à l’authentification :

`models/user.js`

``` js
const bcrypt = require('bcrypt');

// ...

class User extends Model {

  static associate({ AuthToken }) {
    User.hasMany(AuthToken);
  }

  static async authenticate(username, password) {
    const user = await User.findOne({ where: { username } });
    if (user && bcrypt.compareSync(password, user.password)) {
      return user;
    }
    throw new Error('Invalid username or password');
  }

  async authorize() {
    const { AuthToken } = sequelize.models;
    const authToken = await AuthToken.generate(this.id);
    await this.addAuthToken(authToken); // generated method provided by sequelize
    return authToken;
  }

  async logout(token) {
    const { AuthToken } = sequelize.models;
    AuthToken.destroy({ where: { token } });
  }
```

`models/authtoken.js`

``` js
class AuthToken extends Model {

  static associate({ User }) {
    AuthToken.belongsTo(User);
  }

  static async generate(UserId) {
    if (!UserId) {
      throw new Error('AuthToken requires a user ID')
    }
    const token = [ ...Array(20) ].map(() =>
      (~~(Math.random() * 36)).toString(36)
    ).join('');
    return AuthToken.create({ token, UserId })
  }
}
```

### Routes

Afin de tester notre méthode d’authentification, nous allons créer les
routes de notre application qui permettrons à l’utilisateur de
s’authentifier.

Créer un nouveau fichier `routes/auth.js` :

``` js
var express = require('express');
var router = express.Router();
const { User } = require('../models');

// Routes here

module.exports = router;
```

et ajoutons à l’application `app.js` :

``` js
var authRouter = require('./routes/auth');
// ...
app.use('/auth', authRouter);
```

*NOTE*: Toutes les routes auront le préfixe `/auth`

Nous avons besoin de 3 routes pour avoir un système d’authentification
fonctionnel :

  - `GET /auth/login`: Permet d’accèder au formulaire d’authentification

<!-- end list -->

``` js
router.get('/login', async function(req, res, next) {
  res.render('auth/login');
});
```

  - `POST /auth/login`: Méthode d’authentification appelée par le
    formulaire de login

<!-- end list -->

``` js
router.post('/login', async function(req, res, next) {
  const { username, password } = req.body;
  try {
    let user = await User.authenticate(username, password)
    let authToken = await user.authorize();
    res.cookie('auth_token', authToken.token);
    return res.redirect('/');
  } catch (error) {
    return res.render('auth/login', { error });
  }
});
```

  - `DELETE /auth/logout`: Methode permettant de mettre fin à une
    session

<!-- end list -->

``` js
router.delete('/logout', async (req, res) => {
  const { user, cookies: { auth_token: authToken } } = req
  if (user && authToken) {
    await req.user.logout(authToken);
    res.clearCookie('auth_token');
  }
  return res.redirect('/');
});
```

### Views

Il reste à créer le formulaire de login `views/auth/login.ejs` :

``` html
<% if(typeof error !== 'undefined') { %>
  <p class="error">
    <%= error %>
  </p>
<% } %>

<form action="/auth/login" method="POST">
  <input type="text" name="username" />
  <input type="password" name="password" />
  <input type="submit" value="Connexion" />
</form>
```

On peut aussi rajouter un lien vers cette page depuis la page d’accueil
`views/index.js`

``` html
<h1><%= title %></h1>
<p>Welcome to <%= title %></p>
<a href="/auth/login">Connexion</a>
```

On peut déjà (re)démarrer le serveur et verifier que les redirections et
les erreurs fonctionnent :

``` bash
npm start
```

Puis allez à l’adresse <http://localhost:3000>

On remarque que l’application ne fait pas la différence entre
utilisateur connecté et un utilisateur non connecté. Dans les règles de
gestion, certains contenus comme les publications et les commentaires ne
sont accessibles qu’aux utilisateurs connectés. L’application doit donc
être en mesure de discriminer un utilisateur connecté d’un visiteur.
Pour se faire, nous utiliserons un middleware qui récupérera
l’utilisateur depuis la base de donnée et l’ajoutera au contexte de
l’application.

### Middleware

Le middleware fonctionnera de la manière suivante : s’il existe un
cookie ou une en-tête http qui contient un jeton d’authentification,
alors nous essayerons d’identifier l’utilisateur connecté et nous
l’ajouterons au contexte.

`middlewares/auth-middleware.js`

``` js
const { User, AuthToken } = require('../models');

module.exports = async function(req, res, next) {

  const token = req.cookies.auth_token || req.headers.authorization;
  res.locals.currentUser = null;

  if (token) {
    const authToken = await AuthToken.findOne({ where: { token }, include: User });
    if (authToken) {
      req.user = authToken.User;
      res.locals.currentUser = authToken.User
    }
  }
  next();
}
```

Il faut également ajouter ce nouveau middleware dans l’application
`app.js`:

``` js
var authMiddleware = require('./middlewares/auth-middleware.js');
// ...
app.use(authMiddleware);
```

*ATTENTION*: l’ordre de déclaration des middlewares a son importance.

Vous pouvez désormais, personnaliser la page d’accueil lorsque
l’utilisateur est connecté `views/index.ejs`:

``` html
<% if(currentUser){ %>
  <p>Bienvenue <%= currentUser.username %></p>
  <a href="/auth/logout">Déconnexion</a>
<% } else { %>
  <a href="/auth/login">Connexion</a>
<% } %>
```

Redémarrez le serveur pour constater le changement.

### Layout (facultatif, recommandé)

Pour un faciliter la création des nouvelles pages et assurer une
certaine HaRmOnIe DaNs L’InTeRfAcE, nous pouvons nous servir du moteur
`ejs-locals` qui permet l’utilisation de layouts dans les templates :

``` bash
npm install ejs-locals --save
```

Ajoutez ensuite le moteur à l’application `app.js`:

``` js
var engine = require('ejs-locals');
// ...
app.engine('ejs', engine);
// Placer avant :
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
```

Créez ensuite un nouveau fichier `views/application.ejs`, ce sera notre
layout principal :

``` html
<!DOCTYPE html>
<html>
  <head>
    <title>GTD - <%= title %></title>
    <link rel='stylesheet' href='/stylesheets/style.css' />
  </head>
  <body>
    <h1><%= title %></h1>

    <%-body %>

  </body>
</html>
```

Adaptez ensuite les différentes vues de votre application :

`views/index.js`

``` html
<% layout('application') %>

<p>Welcome to <%= title %></p>

<% if(currentUser){ %>
  <p>Bienvenue <%= currentUser.username %></p>
  <a href="/auth/logout" data-method="DELETE">Déconnexion</a>
<% } else { %>
  <a href="/auth/login">Connexion</a>
<% } %>
```

`views/auth/login.js`

``` html
<% layout('../application') %>

<% if(typeof error !== 'undefined') { %>
  <p class="error">
    <%= error %>
  </p>
<% } %>

<form action="/auth/login" method="POST">
  <input type="text" name="username" />
  <input type="password" name="password" />
  <input type="submit" value="Connexion" />
</form>
```

### Assets

Les assets sont des fichiers statiques, generalement compilés, minifiés,
compréssés que l’application va fournir au client :

  - Feuilles de styles (fichiers `.css` compilés de `.sass`,`.scss`, …)
  - Code javascript (fichiers `.js` compilés de `.ts`,`.coffee`, ou
    normes EcmaScript avancées)
  - Images, Icônes
  - Polices de caractères

Nous allons dans un premier temps modifier notre feuille de style par
défaut afin d’afficher les erreurs en rouge:

`public/stylesheets/styles.sass`

``` css
p.error
  color: red
```

Enfin, nous allons corriger rendre la déconnexion fonctionnelle grâce à
un peu de javascript :

`public/javascripts/application.js`

``` js
document.addEventListener('click', async function (e) {
  const { href, dataset } = e.target;
  const { method } = dataset;
  if(method) {
    e.preventDefault();
    const response = await fetch(href, { method, redirect: 'follow' })
    if(response.ok && response.redirected){
      window.location.href = response.url;
    }
    return false;
  }
}, false);
```

# Pour la suite

  - Formulaire d’inscription
  - Page de profil
  - Création d’un post
  - Liste des posts
  - Ajout d’un ami
  - Liste des amis
  - Fil d’actualité

# Pour aller plus loin

## Regles de gestion 2.0

  - Un utilisateur peux réagir avec une émoticone sur un post ou un
    commentaire
  - Il est possible d’attacher une image à un post

# Licence

## CC BY-NC-SA 3.0 FR

[Creative Commons - Attribution - Pas d’Utilisation Commerciale -
Partage dans les Mêmes Conditions 3.0
France](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)
