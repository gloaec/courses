Gestion des données de l’entreprise
================
Ghislain Loaec
09 novembre 2020

## Plan

\columnsbegin
\column{.7\textwidth}
  \tableofcontents
\column{.3\textwidth}

![Application](images/application.png)  

# Introduction

## Un peu d’histoire

*20 ans après leur création, 80% des données sont perdues*

**Pourquoi ?**

  - Destruction ou détérioration des supports
  - Obsolescence matérielle ou logicielle
  - Procédure de stockage indéfinie

**Conséquences**

  - Perte d’information et d’éléments de comparaison
  - Perte de temps
  - Perte de fonds

-----

## Révolution copernicienne de la donnée

Étape 1 : Le souci de l’infrastructure

![Application](images/revolution_1.jpg)  

<!--
La DSI se préoccupe d'abord de fournir aux métiers l'infrastructure nécéssaire
pour qu'ils puissent faire leur travail

=> Architecture en silos
-->

-----

Étape 2 : L’approche qualité de la donnée

![Application](images/revolution_2.jpg)  

<!--
On se préoccupe de plus en plus de l'interopérabilité de la donnée, mais aussi
de sa durée de vie, de la cohérence entre données, du respect du RGPD, des
métadonnées associées.
-->

-----

Étape 3 : La valeur de la donnée

![Application](images/revolution_3.jpg)  

<!--
La gouvernance des données devient indispensable. La valeur apportée à
l'entreprise par la donnée devient le sujet central. Tous les services sont
concernés. Il faut désormais construire une stratégie d'entreprise s'appuyant
sur cette valeur de la donnée.

=> Master data management
-->

## Les états de la donnée

![Application](images/data_states.jpg)  

## Naissance du big data

Le **big data** désigne des ensembles de données devenus si
**volumineux** qu’ils dépassent l’**intuition**, les capacités humaines
d’analyse et même celles des outils informatiques **classiques** de
gestion de base de données ou de l’information

## Big data

3 zettaoctets de données transmises par jour

![Application](images/octet_table.png)  

## L’or noir du 21e siècle

Liste des entreprises par capitalisation boursière

\columnsbegin
\column{.37\textwidth}

2009  
  
![Application](images/stocks_2009.png)   2020  
  
![Application](images/stocks_2020.png)  

## 

![](images/best_data.jpg)

## Gouvernance vs Gestion des données

\columnsbegin
\column[T]{.5\textwidth}

**Gouvernance des données**

*Une stratégie d’entreprise*

Regroupe les procédures permettant d’assurer la qualité des données au
sein de l’entreprise. Elle consiste à maintenir des données correctes,
fiables, actualisées et sécurisées.

<!--
  A pour objectif de fournir des réponses concrètes à la manière dont une
  entreprise peut déterminer et hiérarchiser les avantages financiers des données
  tout en atténuant les risques commerciaux liés aux données de mauvaise qualité.

  La gouvernance des données nécessite de déterminer quelles données peuvent être
  utilisées dans quels scénarios. Ce qui nécessite de déterminer exactement ce
  que sont des données acceptables :

    - qu’est-ce que les données ?
    - où sont-elles collectées et utilisées ?
    - quelle doit être leur précision ?
    - quelles règles doivent-elles suivre ?
    - qui est impliqué dans diverses parties des données ?

  La gouvernance des données doit aller au-delà de l’informatique et inclure
  des parties prenantes de l’ensemble de l’entreprise.  Afin de garantir la
  sécurité, et la fiabilité de toutes les données, la gouvernance exige la
  participation des parties prenantes de tous les secteurs d’activité.
  Considérez l’alternative : si chaque silo d’entreprise aborde sa stratégie de
  données de manière différente, le résultat final est chaotique et,
  probablement, pas assez complet pour être utile.
  -->

\column[T]{.5\textwidth}

**Gestion des données**

*Une pratique informatique*

Ensemble complet et personnalisé de pratiques, théories, processus et
systèmes (une suite complète d’outils) permettant de collecter, valider,
stocker, organiser, protéger, traiter et sinon maintenir les données.

<!--
  Le but est d'organiser et de contrôler les ressources de  données de manière
  à ce qu’elles soient accessibles, fiables et opportunes
  -->

\columnsend

<!--
La data governance constitue ainsi la base de la stratégie de gestion des
données de l’entreprise. Le data management et la data governance sont des
initiatives globales qui s’inscrivent sur du long terme.

La data management est étroitement lié à la data governance. Une stratégie de
data management passe obligatoirement par la mise en place d'une gouvernance
des données en amont.
-->

# Gouvernance des données

## Le DSD (*Directeur Systèmes de Données*)

  - **CDO** (*Chief Data Officer*) ou **Data steward**

Personne en charge du déploiement et de l’application des **politiques
de gestion des données** à l’échelle de l’entreprise :

  - Collectes de données
  - Mouvements de données
  - Meilleurs pratiques d’implémentation
  - Respect des règles

<!--
Le data steward n’est pas responsable de l’élaboration des politiques de
gestion des données, mais de leur déploiement et application à l’échelle de
l’entreprise : politiques relatives à la collecte et aux mouvements des
données, implémentation des meilleures pratiques, respect des règles.
-->

## Procédures documentées

  - **PRA** (Plan de reprise d’activité) <!--
      Prévenir les 3 types de sinistres informatiques :
      - Catastrophes naturelles
        - Perte d’un immeuble ou d’un local (coupure elec, feu, tempete)
        - Désastre à grande échelle (cyclones, tremblements de terres)
      - Sinistres sur les installations
        - Vols, attentats sabotages
        - Incendie, dégâts des eaux
      - Cybercriminalité et malveillance
        - Virus, Cyberattaque, Piratage informatique, Logiciels malveillants
        - Perte données, communication, saturation des infra, panique système ou un
          arrêt des activités
      -->
      - Mesures préventives <!--
        Identifier et réduire les risques :
        - Des sauvegardes de données très régulières,
        - De prévoir un plan de reprise d'activité précis et testé régulièrement.
        - D’installer des générateurs, onduleurs et de conduire des inspections au quotidien.
        -->
      - Mesures détectives <!--
        Détecter la présence d’éléments indésirables dans le système d’information de
        l’organisation. Leur but est de découvrir de nouvelles menaces potentielle
        - Installer des logiciels anti-virus à jour et les renouveler,
        - Mettre en place des sessions de formation des salariés pour diminuer le
          phénomène de shadow IT (applis en scrèd dans l'infra)
        - Installer des logiciels de contrôle/surveillance des serveurs et des réseaux,
        -->
      - Mesures correctives <!--
        Restaurer un système après un événement indésirable (sinistre, désastre). Ces
        mesures portent sur la fixation ou la restauration des systèmes d’information
        après l’incident. Pour chaque mesure :
        - Quel est son objectif et son but ?
        - Quelles sont les personnes ou équipes responsables si des perturbations surviennent?
        - Que feront ces personnes quand l’incident surviendra ?
        -->
  - **PCA** (*Plan de Continuité des Activités*) <!--
      A pour but de garantir la survie de l'entreprise en cas de sinistre
      important touchant le système informatique. Il s'agit de redémarrer
      l'activité le plus rapidement possible avec le minimum de perte de données
      -->
      - Mesures préventives <!--
        - La sauvegarde des données
        - Les systèmes de secours
        - Une bonne information et un bon partage des rôles
        -->
      - Mesures curatives <!--
        - La reprise des données (données perdues => restaurations)
        - Le redémarrage des applications (panne => sites de secours)
        - Le redémarrage des machines (provisoire ou définitif, basculer les
          utilisateurs, si possible sans déconnexion)
        -->
  - **PGD** (*Plan de Gestion des Données*) <!--
      Plutot applicable au domaine académique. Document évolutif qui aide les
      chercheurs ou le chargé de projet de la recherche à définir un plan pour
      gérer les données utilisées et générées dans le cadre de son activité ou de
      son projet de recherche. Il suit le cycle de vie de la donnée, de la création
      à la destruction.
      -->
      - Données de recherche

## Qualité des données

<!--
C'est la base du data management. La non-qualité des données représente un
risque et des coûts important pour l’entreprise. Des données incorrectes pas à
jour joue sur la connaissance client. La réactivité de l’entreprise face à la
réalité est plus lente. Les actions mises en place à partir de
données non qualitatives sont fausses. Les décisions prises sont directement
impactées.
-->

  - Responsable qualité des données <!--
    On peut voir le data steward comme un policier qui fait régner l'ordre dans le
    monde numérique, lui c'est un peu comme le greffier du tribunal.
    Chargé d'examiner les données collecter et déceler des problèmes :
    - Enregistrement en doubles
    - Version incoherentes
    -->
  - Procédures claires de qualification au sein de l’organisation pour
    garantir
      - l’exactitude des données
      - la cohérence des données
      - la traçabilité des données

## Sécurisation des données

<!--
Étroitement liée à la qualité, la sécurisation des données est également
un enjeu important du data management.

La sécurisation des données informatiques a toujours été un point
important pour les entreprises. Elle s’est intensifiée avec l’arrivée du
Big Data. Le data management doit assurer la confidentialité et la
sécurité des données au sein des infrastructures IT de l'organisation.
-->

\columnsbegin
\column{.6\textwidth}

  - DevSecOps <!--
      Pratique émergente qui intègrent des mécanismes de sécurité à tous les
      niveaux du développement. La sécurité DevOps est conçue pour les conteneurs
      et les microservices
      -->
      - Sécurité de l’environnement et des données <!--
        - Standardiser et automatiser l'environnement (conteneurs avec un minimum de privilèges)
        - Sécurité des processus CI/CD
        - Centraliser les capacités de gestion des identités des utilisateurs et de
          contrôle d'accès (authentification multiples => LDAP, CAS, OAuth, Shiboleth, OpenID)
        - Isoler les conteneurs qui exécutent des microservices les uns des autres et du réseau
        - Chiffrer les données entre les applications et les services
        - Utiliser des passerelles d'API sécurisées
        -->
      - Sécurité des processus CI/CD <!--
        - Intégrer des analyseurs de sécurité pour les conteneurs (ca devrait faire
          partie du processus d'ajout de conteneurs au registre)
        - Automatiser les tests de sécurité dans le processus CI (identifier les
          failles de sécurité connues)
        - Ajouter des tests de sécurité automatisés (authentification, payment, etc)
        - Automatiser les mises à jour de sécurité (versions PATCH, correctifs pour
          les failles connues)
        - Automatiser les capacités de gestion de configuration des systèmes et des
          services (Ansible, Chef) => evite les erreurs manuelles
        -->
  - Spécialistes de la sécurité <!--
      Toujours responsables d’un certain nombre d’opérations
      -->
      - Gestion du cryptage
      - Prévention des accès non autorisés
      - Protection contre les suppressions ou mouvements accidentels

\column{.4\textwidth}

![](images/devsecops.png)  

\columnsend

<!--
L’un des aspects les plus importants de la gestion des données est la
applications et des échanges de données, les spécialistes de la sécurité
sont toujours responsables d’un certain nombre d’opérations, dont gestion
du cryptage, prévention des accès non autorisés, protection contre les
mouvements ou suppressions accidentels des données et autres
préoccupations de premier plan.
-->

## Référentiel de données

  - **MDM** (*Master Data Management*)

<!--
Le MDM vise à définir et gérer les données de référence de l'entreprise au sein
d'un entrepôt unique. Les données de référence sont les informations
stratégiques :
-->

Regrouper en un entrepôt unique les données partagées entre les services
:

  - les informations sur les clients,
  - les produits
  - les ressources
  - *etc.*

<!--
Permet de s’assurer que l’entreprise travaille toujours avec une version
unique, actualisée et fiable des données, ce qui lui permet de prendre des
décisions plus avisées.

Exige des outils adaptés, parce qu'il faut
- importer de différentes sources
- présenter sous forme cohérente et fiable
- propager dans différents systèmes
-->

## Conformité aux obligations légales

<!--
Le cadre réglementaire autour de la donnée s’est intensifié ces trois
dernières années. Les entreprises ont été largement sensibilisées au RGPD, qui
favorise un usage éthique et une traçabilité des données au sein des
entreprises.

-->

  - **RGPD** (*Règlement Général sur la Protection des Données
    Personnelles*)

Documenter la conformité :

  - Traitements de données personnelles
      - **Registre des traitements**
        <!-- ou des catégories d’activités de traitements (pour les sous-traitants) -->
      - **AIPD** (*Analyses d’Impact Relatives à la Protection des
        Données*
        <!-- pour les traitements susceptibles d'engendrer des risques élevés pour
        les droits et libertés des personnes -->
      - **L’encadrement des transferts** de données hors de l’Union
        européenne
        <!-- notamment, les clauses contractuelles types, les BCR et certifications) -->
  - L’information des personnes <!--
      - **Les mentions d'informations**
      - Les modèles de **recueil du consentement des personnes concernées**
      - Les procédures mises en place pour l'**exercice des droits**
      -->
  - Les contrats qui définissent les rôles et les responsabilités des
    acteurs <!--
      - Les contrats avec les sous-traitants
      - Les procédures internes en cas de violations de données
      - Les preuves que les personnes concernées ont donné leur consentement
        lorsque le traitement de leurs données repose sur cette base.
      -->

## Optimisation des infrastructures

<!--
Sans doute l'enjeu principal du data management.  Les entreprises ont construit
leurs architectures en silos de données depuis des années. L’enjeu est
aujourd’hui de sortir de cette organisation segmentée et de se diriger vers des
plateformes globales de données.

Le big data est un terme fourre-tout utilisé en parallèle avec des
pratiques qui visent à améliorer les opérations de l’entreprise :
collecte, analyse et exploitation de volumes considérables de données
-->

  - Repenser les systèmes d’information
      - Accessibilité des métiers (synchronisation)
      - Respect des réglementations
      - Contraintes de sécurité
      - Impact écologique

## Performance de l’entreprise

<!--
Enfin, les données sont un facteur direct de la performance de l'entreprise.
Les données doivent guider les décideurs dans la prise de décisions et
l’action.

Les données sont la ressource la plus précieuse des entreprises
performantes.
Défi: Comment gérer ces données efficacement ?
-->

  - **BI** (*Business Intelligence*) ou Informatique décisionnelle

Ensemble des moyens, des outils et des méthodes qui permettent de
collecter, consolider, modéliser et restituer les données, matérielles
ou immatérielles, d’une entreprise en vue d’offrir une aide à la
décision et de permettre à un décideur d’avoir une vue d’ensemble de
l’activité traitée.

## 

# Gestion des données

## Structures de données

Permet d’**organiser** les données pour faciliter leur **traitement**.

\columnsbegin
\column[T]{.33\textwidth}

XML

\tiny

``` xml
<users>
  <user>
    <firstname>Jean</firstname>
    <lastname>Neige</lastname>
  </user>
  <user>
    <firstname>Jorah</firstname>
    <lastname>Friendzone</lastname>
  </user>
</users>
```

\column[T]{.33\textwidth}

JSON

\tiny

``` json
{
  users: [
    {
      firstname: 'Jean',
      lastname: 'Neige'
    },
    {
      firstname: 'Jorah',
      lastname: 'Friendzone'
    }
  ]
}
```

\column[T]{.33\textwidth}

YAML

\tiny

``` json
users:
  - firstname: 'Jean'
    lastname: 'Neige'
  - firstname: 'Jorah'
    lastname: 'Friendzone'
```

\columnsend

## Bases de données

Permet de **stocker** et de **retrouver** des **données brutes** ou des
**informations**.

  - SGBD **Relationnels**
      - **SQL**: PostgreSQL, MySQL, Oracle, SQLServer, SQLite
  - SGBD **NoSQL**
      - **Clé-valeur**: Voldemort, Redis, Riak (Dynamo)
      - **Colonne**: HBase, Cassandra, SimpleDB, Hypertable
      - **Document**: CouchDB, MongoDB, RavenDB, Terrastore
      - **Graphe**: Neo4j, OrientDB, GraphDB

<!--
- Clé valeur :
  - Voldemort (libéré par LinkedIn)
  - Redis (sponsorisé par VMWare)
  - Riak (implémentation Open-Source de Amazon Dynamo)
- Colonne :
  - HBase (version Open-Souce de BigTable par Google)
  - Cassandra (fondation Apache, basé sur Amazon Dynamo, né chez Facebook)
  - SimpleDB (Amazon)
  - Hypertable
- Document :
  - CouchDB (fondation Apache)
  - MongoDB
  - RavenDB (plateformes « .NET/Windows » - LINQ)
  - CouchBase
  - Terrastore
- Graphe :
  - Neo4j
  - OrientDB (fondation Apache)
  - GraphDB (facebook)
-->

## 

## Modèle relationnel

Type entité-association

![Application](images/relational_database.jpg)  

-----

  - **Index**: Accélère l’accès à l’information grâce à un mécanisme de
    pointeurs
      - **Hashing** ou Table de hachage: Tri les données grâce à une
        opération arithmétique sur leurs valeurs (accès plus rapide)
      - **B-Tree** ou Arbre B: Organise les données grâce à un index
        sous forme d’arbre équilibré (accès rapide à l’information)
      - **Bitmap**: Organise les données grâce à un index sous forme de
        tableau de bits (utilisé pour collections avec peu de valeurs)

-----

  - **Partitionnement**: Répartit les données sur plusieurs disques
    (accès plus rapide, moins de charge disque)
  - **RAID** (*Redundant Array of Inexpensive Disks*): Duplique les
    données sur plusieurs disques (accès plus rapide, évite la perte
    d’informations)
  - **ISAM** (*Indexed Sequential Access Method*): Segmente les données
    dans des cylindres avec un arbre trié par clé primaire (accélère la
    lecture mais aussi l’éctriture)

-----

  - **View** ou Vue matérialisée: Enregistre le résultat d’une requête
    pour utilisation ultérieure
  - **Function**: Permet une opération sur les données (retourne une
    valeur, paramètres non-modifiables)
  - **Procedure**: Permet de compiler une suite d’opérations sur les
    données
  - **Trigger**: Permet de réagir aux changements des données
  - **Logs** ou Journal d’évenements: Liste des dernières opérations
    effectuées sur la base de données (permet d’annuler et de rejouer
    des opérations)

## Modèles NoSQL

Type **Key-Value** (Clé-valeur)

![](images/key_value.png)  

<!--
  Basique, chaque objet est identifié par une clé unique constituant la seule
  manière de le requêter

  Elles fonctionnent comme un grand tableau associatif et retourne une valeur
  dont elle ne connaît pas la structure

  -  leur modèle peut être assimilé à une table de hachage (hashmap)
     distribuée
  -  les données sont simplement représentées par un couple clé/valeur
  -  la valeur peut être une simple chaîne de caractères , ou un objet
     sérialisé...
  -  cette absence de structure ou de typage ont un impact important sur le
     requêtage : toute l’intelligence portée auparavant par les requêtes SQL
     devra être portée par l’applicatif qui interroge la BD.

  Chaque objet est identifié par une clé unique seule façon de le requêter
  La structure de l’objet est libre , souvent laissé à la charge du
  développeur de l’application (XML, JSON, ...), la base ne gérant généralement
  que des chaînes d’octets
-->

## Modèles NoSQL

Type **Column** (Colonne)

![](images/column.png)  

<!--
Permet de disposer d'un très grand nb de valeurs sur une même ligne, de
stocker des relations "one-to-many" , d’effectuer des requêtes par clé
(adaptés au stockage de listes : messages, posts, commentaires, ...)

- Les données sont stockées par colonne , non par ligne
- on peut facilement ajouter des colonnes aux tables, par contre l'insertion
d'une ligne est plus coûteuse
- quand les données d'une colonne se ressemblent, on peut facilement
compresser la colonne
- modèle proche d’une table dans un SGBDR mais ici le nombre de colonnes
    - est dynamique
    - peut varier d’un enregistrement à un autre ce qui évite de retrouver des
      colonnes ayant des valeurs NULL.
-->

## Modèles NoSQL

Type **Document**

![](images/document.png)  

<!--
Pour la gestion de collections de documents, composés chacun de champs et de
valeurs associées, valeurs pouvant être requêtées (adaptées au stockage de
profils utilisateur)

- Elles stockent une collection de "documents"
- elles sont ba sé es sur le modèle « clé - valeur » mais la valeur est un
  document en format semi - structuré hiérarchique de type JSON ou XML
  ( possible aussi de stocker n'importe quel objet, via une sérialisation)
- les documents n'ont pas de schéma, mais une structure arborescente : ils
  contiennent une liste de champs, un champ a une valeur qui peut être une
  liste de champs, ...
- elles ont généralement une interface d’accès HTTP REST permettant d’effectuer
des requêtes (plus complexe que l’interface CRUD des BD clés/valeurs)
- Implémentations les plus connues
-->

## Modèles NoSQL

Type **Graphe**

![](images/graphe.png)  

<!--
Pour gérer des relations multiples entre les objets (adaptés au données
issues de réseaux sociaux, ...)

- Elles permettent la modélisation , le stockage et la manipulation de données
  complexes liées par des relations non-triviales ou variables
- Modèle de représentation des données basé sur la théorie des graphes
- S’appuie sur les notions de noeuds , de relations et de propriétés qui leur
  sont rattachées.
-->

-----

  - **Sharding** Partitionnement des données sur plusieurs serveurs
  - **Consistent hashing** Partitionnement des données sur plusieurs
    serveurs eux-mêmes partitionnés sur un segment
  - **Map Reduce** Modèle de programmation parallèle permettant de
    paralléliser tout un ensemble de tâches à effectuer sur un ensemble
    de données
  - **MVCC** “Contrôle de Concurrence Multi-Version”: mécanisme
    permettant d’assurer le contrôle de concurrence
  - **Vector-Clock** ou horloges vectorielles: Permet des mises à jours
    concurentes en datant les données par des vecteurs d’horloge.

# L’approche relationnelle

## Analyse conceptuelle

\columnsbegin
\column{.4\textwidth}

Résulte de l’observation de la réalité que l’on va chercher à
**modéliser**.

Elle commence par l’élaboration de **règles de gestion** à partir des
processus **métiers**

\column{.6\textwidth}

![](images/data_analysis.jpg)  

\columnsend

-----

### Règles de gestion

*Exemple*:

  - L’application comporte un ensemble de personnages
  - Les personnages ont tous un prénom, mais parfois pas de nom
  - Certains personnages sont nobles et par conséquent ont plusieurs
    titres honorifiques
  - Les nobles et seuls les nobles peuvent gouverner des provinces
  - Chaque personnage a 2 parents, mais il ne sont pas toujours connus
  - Les personnages peuvent se marier, mais ne peuvent pas cummuler les
    mariages
  - Seul un roi peut épouser sa tante

## Modèle conceptuel

  - L’ensemble des données concernant une même catégorie d’individus ou
    d’objets est regroupé dans des **entités**.
  - Des liens unissent les entités entre elles : ces liens constituent
    des **associations**.

## Modèle logique

Consiste à décrire la **structure de données** utilisée sans faire
référence à un langage de programmation. Il s’agit donc de préciser
les **types de données** utilisées lors des traitements

## Exemple

  - L’application comporte un ensemble de personnages

![](images/mld1.jpg)  

-----

  - Les personnages ont tous un prénom, mais parfois pas de nom

![](images/mld2.jpg)  

-----

  - Certains personnages sont nobles et par conséquent ont plusieurs
    titres honorifiques

![](images/mld3.jpg)  

-----

  - Les nobles et seuls les nobles peuvent gouverner des provinces

![](images/mld4.jpg)  

-----

  - Chaque personnage a 2 parents, mais il ne sont pas toujours connus

![](images/mld5.jpg)  

-----

  - Les personnages peuvent se marier, mais ne peuvent pas cummuler les
    mariages

![](images/mld6.jpg)  

-----

  - Seul un roi peut épouser sa tante

![](images/mld7.jpg)  

-----

## Modèle logique

![](images/mld8.jpg)  

## SQL: Select

Tous les utilisateurs :

``` sql
SELECT * FROM users
```

Tous les administrateurs :

``` sql
SELECT * FROM users WHERE role="admin"
```

Utilisateur avec l’identifiant `1` :

``` sql
SELECT * FROM users WHERE id=1 LIMIT 1
```

## 

![](images/select.png)

## PHP: Select

Tous les produits :

``` php
$product_id = $_GET['product_id'];
$query      = "SELECT * FROM products "
            . "WHERE id='$product_id'");
$results    = mysql_query($query);
```

## WOW: Security Alert \!

``` php
$_GET['product_id'] = " ' UNION
  SELECT username, password
  FROM users
";
```

Toujours traiter les données entrantes :

``` php
$product_id = $_GET['product_id'];
$query      = "SELECT * FROM products "
            . "WHERE id='"
            . mysql_real_escape_string($product_id)
            . "'");
$results    = mysql_query($query);
```

## ORM

  - `php` -\> `Doctrine` (Symfony), `CakePHP`, `Laravel`
  - `ruby` -\> `ActiveRecord` (RoR), `DataMapper`
  - `python` -\> `SQLAlchemy`, `Peewee`, `Django`
  - `node` -\> `Squeletize`, `(Mongoose)`

## ORM: Symfony

Afficher le nom de tous les utilisateurs :

``` php
$users = Doctrine::getTable('users')->findAll();
foreach($users as $user)
  echo "<h2>$user->username</h2>";
```

-----

Récupérer le nom d’une categorie :

``` php
function getCategoryName($category_id) {
  $q = Doctrine_Query::CREATE()
  ->select('name')
  ->from('Category c')
  ->where('c.category_id = ?',$category_id);
  return $q->fetchOne();
}
```

-----

Compter le nombre de numéros de téléphones pour chaque utilisateur :

``` php
$q = Doctrine_Query::create()
    ->select('u.*, COUNT(DISTINCT p.id) AS nb')
    ->from('User u')
    ->leftJoin('u.Phonenumbers p')
    ->groupBy('u.id');
$users = $q->fetchArray();

echo $users[0]['Phonenumbers'][0]['nb'];
```

## Model

`$ php bin/console make:entity Product`

`src/Entity/Product.php`

\scriptsize

``` php
use Doctrine\ORM\Mapping as ORM;

class Product {
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=100)
   */
  private $name;
}
```

## Model: Getters / Setters

\scriptsize

``` shell
$ php bin/console doctrine:generate:entities     # symfony < 4
```

`src/Entity/Product`

\scriptsize

``` php
class Product {
  ...

  public function getId() {
      return $this->id;
  }

  public function getName() {
      return $this->name;
  }

  public function setName($name) {
      $this->name = $name;
  }
}
```

## Model: Migrations

\scriptsize

``` shell
$ php bin/console doctrine:migrations:diff
$ cat /path/to/project/doctrine/src/Migrations/Version20171122151511.php
```

\scriptsize

``` php
class Version20180104140143 extends AbstractMigration {

  public function up(Schema $schema) {
      $this->addSql('CREATE TABLE `Product` (...)');
  }

  public function down(Schema $schema) {
      $this->addSql('DROP TABLE `Product`');
  }
}
```

\scriptsize

``` shell
$ php bin/console doctrine:migrations:migrate
```

## 

![](images/i_know_sql.jpg)

# Questions

<!--
  vim: spell spelllang=fr
-->
